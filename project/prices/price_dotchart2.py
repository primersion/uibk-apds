# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import csv
import codecs
import sys
from os.path import isfile
import os
from glob import glob
import matplotlib.cm as cm

linewidth = 1

if len(sys.argv) < 2:
	print("Usage: python %s <pricelist>" % sys.argv[0])
	sys.exit(0)

if len(sys.argv) > 2:
	linewidth = int(sys.argv[2])

plt.figure(figsize=(15,12))

prices = {}

regionLabels = []

with codecs.open(sys.argv[1], "r", encoding='utf-8', errors='ignore') as csvFile:
  csvReader = csv.reader(csvFile, delimiter=',')

  for row in csvReader:
    if row[0] not in regionLabels:
      regionLabels.append(row[0])
    if row[1] in prices:
      prices[row[1]].append(row[2])
    else:
      prices[row[1]] = [row[2]]

colors = ['C0','C1','C2','C3','C4','C5','C6','C7','C8','C9']
iCount = 0
for (instanceType, price) in prices.items():
  plt.plot(range(0, len(price)), price, colors[iCount], label=instanceType, ms=5, linewidth=linewidth)
  iCount += 1

plt.xlabel("Region")
plt.ylabel("Price in $")
plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)
plt.xticks(range(0, len(regionLabels)), regionLabels)
plt.savefig('instance_prices2.png')
plt.show()
