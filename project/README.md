### Directory structure

Multi-Objective Cloud Broker that we have developed throughout the proseminar. This contents are structured as follows:

- **architecture** - contains information about the architecture that we have used for our system
- **benchmark** - contains all benchmark data that we have collected for the evaluation of our system and plots of the benchmark
- **CloudBroker** - Eclipse project containing all code we have developed
- **code-snippes** - contains code snippets we have initially developed for fetching AMIs and price list from amazon
- **evaluation** - contains rankings of our CloudBroker for different user inputs using the benchmark data and plots of the data
- **log** - contains results of the benchmark using only t2.micro instances for the purpose of testing
- **notes** - contains some notes collected throughout the project
- **papers** - contains some papers that we have used for our work
- **ping** - contains the results of a one week long ping evaluation of the amazon servers and plots of the data
- **presentation** - contains all presentations we have created
- **prices** - contains amazon pricelists and plots of them
- **report** - contains the report of our project


### Running the CloudBroker
To try out our Cloud Broker import the 'CloudBroker' project into eclipse. Make sure that you have your AWS credentials set up. Next copy the content of the 'log' folder (only t2.micro instances) or the content of one of the folders in 'benchmark/results_uibk' to a folder called 'log' inside your home directory. You can now run the class CloudBroker.java and follow the instructions. Make sure that if you are creating new benchmark data, you have sufficient funds on your Amazon account or set testMode inside the CloudBroker class to true (only use t2.micro instances). Otherwise you will be charged $$$.
