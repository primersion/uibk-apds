# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import csv
import codecs
import sys
from os.path import isfile
import os
from glob import glob
import matplotlib.cm as cm

if len(sys.argv) < 2:
	print("Usage: python %s <data>" % sys.argv[0])
	sys.exit(0)

timestamps = sorted(glob(sys.argv[1] + "/*/"))

instanceLabels = []
regionLabels = []
tIndex = 0

regionExec = []

for t in timestamps:
  regions = sorted(glob(t + "/*/"))
  for r in regions:
    label = r.split(os.sep)[2]
    if label not in regionLabels:
      regionLabels.append(label)
      regionExec.append([])
    rIndex = regionLabels.index(label)
      
    instances = sorted(glob(r + "/*/"))
    iIndex = 0
    if len(instanceLabels) == 0:
      for i in instances:
        instanceLabels.append(i.split(os.sep)[3])
    if len(regionExec[rIndex]) == 0:
      for i in instances:
        regionExec[rIndex].append([])
    for i in instances:
      logs = glob(i + "*")
      
      avgExecTime = 0
      avgCpu = 0
      avgMem = 0
      avgFreeMem = 0
      for l in logs:
        cpu = 0
        mem = 0
        freeMem = 0
        firstEpoch = 0
        lastEpoch = 0
        with codecs.open(l, "r", encoding='utf-8', errors='ignore') as csvFile:
          csvReader = csv.reader(csvFile, delimiter=',')
          
          for i in range(0, 7):
            next(csvReader)
          
          count = 0
          for row in csvReader:
            if count == 0:
              firstEpoch = float(row[0])
            lastEpoch = float(row[0])
            cpu += float(row[1])
            mem += float(row[7]) / 10000000
            freeMem += float(row[10]) / 10000000
            count += 1
        
        avgExecTime += lastEpoch - firstEpoch
        avgCpu += cpu / count
        avgMem += mem / count
        avgFreeMem += freeMem / count
      
      numLogs = len(logs)
      avgExecTime /= numLogs
      avgCpu /= numLogs
      avgMem /= numLogs
      avgFreeMem /= numLogs
      
      regionExec[rIndex][iIndex].append(avgExecTime)
      
      iIndex += 1
    rIndex += 1
  tIndex += 1

for i in range(0, len(instanceLabels)):
  data = []
  for r in range(0, len(regionExec)):
    data.append(regionExec[r][i])
  plt.figure(figsize=(15,12))
  plt.boxplot(data)
  plt.ylabel("Average execution time in s")
  xTicks = range(1, len(regionLabels) + 1)
  plt.xticks(xTicks, regionLabels)
  plt.title(instanceLabels[i])
  plt.savefig(instanceLabels[i] + ".png")
  plt.cla()
