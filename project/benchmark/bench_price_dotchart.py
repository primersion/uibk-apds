# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import csv
import codecs
import sys
from os.path import isfile
import os
from glob import glob
import matplotlib.cm as cm

if len(sys.argv) < 3:
	print("Usage: python %s <bench data> <pricelist>" % sys.argv[0])
	sys.exit(0)

plt.figure(figsize=(15,12))

timestamps = sorted(glob(sys.argv[1] + "/*/"))

maxExec = 0
regionY = []
execTimeX = []
instanceLabels = []
regionLabels = []
tIndex = 0

avgExecTotal = []

for t in timestamps:
  regions = sorted(glob(t + "/*/"))
  rIndex = 0
  if len(regionLabels) == 0:
    for r in regions:
      regionLabels.append(r.split(os.sep)[2])
  for r in regions:
    instances = sorted(glob(r + "/*/"))
    iIndex = 0
    if len(instanceLabels) == 0:
      for i in instances:
        instanceLabels.append(i.split(os.sep)[3])
    regionAvg = []
    for i in instances:
      regionAvg.append(0)
    avgExecTotal.append(regionAvg)
    for i in instances:
      logs = glob(i + "*")
      
      avgExecTime = 0
      avgCpu = 0
      avgMem = 0
      avgFreeMem = 0
      for l in logs:
        cpu = 0
        mem = 0
        freeMem = 0
        firstEpoch = 0
        lastEpoch = 0
        with codecs.open(l, "r", encoding='utf-8', errors='ignore') as csvFile:
          csvReader = csv.reader(csvFile, delimiter=',')
          
          for i in range(0, 7):
            next(csvReader)
          
          count = 0
          for row in csvReader:
            if count == 0:
              firstEpoch = float(row[0])
            lastEpoch = float(row[0])
            cpu += float(row[1])
            mem += float(row[7]) / 10000000
            freeMem += float(row[10]) / 10000000
            count += 1
        
        avgExecTime += lastEpoch - firstEpoch
        avgCpu += cpu / count
        avgMem += mem / count
        avgFreeMem += freeMem / count
      
      numLogs = len(logs)
      avgExecTime /= numLogs
      avgCpu /= numLogs
      avgMem /= numLogs
      avgFreeMem /= numLogs
      
      avgExecTotal[rIndex][iIndex] += avgExecTime
      
      if len(execTimeX) < iIndex + 1:
        execTimeX.append([avgExecTime])
      else:
        execTimeX[iIndex].append(avgExecTime)
        
      if len(regionY) < iIndex + 1:
        regionY.append([rIndex * (len(timestamps) + 1) + tIndex])
      else:
        regionY[iIndex].append(rIndex * (len(timestamps) + 1) + tIndex)
      
      if avgExecTime > maxExec:
        maxExec = avgExecTime
      iIndex += 1
    rIndex += 1
  tIndex += 1
  
for i in range(0, len(regionLabels)):
  for j in range(0, len(instanceLabels)):
    avgExecTotal[i][j] /= len(timestamps)
  
prices = {}

priceRegionLabels = []

with codecs.open(sys.argv[2], "r", encoding='utf-8', errors='ignore') as csvFile:
  csvReader = csv.reader(csvFile, delimiter=',')

  for row in csvReader:
    if row[0] not in regionLabels:
      continue
    if row[0] not in priceRegionLabels:
      priceRegionLabels.append(row[0])
    if row[1] in prices:
      prices[row[1]].append(float(row[2]))
    else:
      prices[row[1]] = [float(row[2])]
      
for j in range(0, len(instanceLabels)):
  for i in range(0, len(regionLabels)):
    regionIndex = priceRegionLabels.index(regionLabels[i])
    prices[instanceLabels[j]][regionIndex] *= avgExecTotal[i][j] / 3600
          
colors = ['C0o','C1o','C2o','C3o','C4o','C5o','C6o','C7o','C8o','C9o']
iCount = 0
for (instanceType, price) in prices.items():
  plt.plot(range(0, len(price)), price, colors[iCount], label=instanceType, ms=5)
  iCount += 1

plt.xlabel("Region")
plt.ylabel("Price in $")
plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)
plt.xticks(range(0, len(priceRegionLabels)), priceRegionLabels)
plt.savefig('bench_with_prices.png')
plt.show()
