# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import csv
import codecs
import sys
from os.path import isfile
import os
from glob import glob
import matplotlib.cm as cm

if len(sys.argv) < 2:
	print("Usage: python %s <data>" % sys.argv[0])
	sys.exit(0)

plt.figure(figsize=(15,12))

timestamps = sorted(glob(sys.argv[1] + "/*/"))

maxExec = 0
regionY = []
execTimeX = []
instanceLabels = []
regionLabels = []
tIndex = 0

regionCounts = {}
for t in timestamps:
  regions = sorted(glob(t + "/*/"))
  for r in regions:
    label = r.split(os.sep)[2]
    if label not in regionCounts:
      regionCounts[label] = 1
    else:
      regionCounts[label] += 1

regionPrefixSum = {}
curPrefixSum = 0
for (region, count) in regionCounts.items():
  regionPrefixSum[region] = curPrefixSum
  curPrefixSum += count

nextRegionYIndex = {}
for t in timestamps:
  regions = sorted(glob(t + "/*/"))
  
  for r in regions:
    label = r.split(os.sep)[2]
    if label not in regionLabels:
      regionLabels.append(label)
      nextRegionYIndex[label] = 0
    rIndex = regionLabels.index(label)
    
    instances = sorted(glob(r + "/*/"))
    iIndex = 0
    if len(instanceLabels) == 0:
      for i in instances:
        instanceLabels.append(i.split(os.sep)[3])
    for i in instances:
      logs = glob(i + "*")
      
      avgExecTime = 0
      avgCpu = 0
      avgMem = 0
      avgFreeMem = 0
      for l in logs:
        cpu = 0
        mem = 0
        freeMem = 0
        firstEpoch = 0
        lastEpoch = 0
        with codecs.open(l, "r", encoding='utf-8', errors='ignore') as csvFile:
          csvReader = csv.reader(csvFile, delimiter=',')
          
          for i in range(0, 7):
            next(csvReader)
          
          count = 0
          for row in csvReader:
            if count == 0:
              firstEpoch = float(row[0])
            lastEpoch = float(row[0])
            cpu += float(row[1])
            mem += float(row[7]) / 10000000
            freeMem += float(row[10]) / 10000000
            count += 1
        
        avgExecTime += lastEpoch - firstEpoch
        avgCpu += cpu / count
        avgMem += mem / count
        avgFreeMem += freeMem / count
      
      numLogs = len(logs)
      avgExecTime /= numLogs
      avgCpu /= numLogs
      avgMem /= numLogs
      avgFreeMem /= numLogs
      
      if len(execTimeX) < iIndex + 1:
        execTimeX.append([avgExecTime])
      else:
        execTimeX[iIndex].append(avgExecTime)
        
      if len(regionY) < iIndex + 1:
        regionY.append([regionPrefixSum[label] + rIndex + nextRegionYIndex[label]])
      else:
        regionY[iIndex].append(regionPrefixSum[label] + rIndex + nextRegionYIndex[label])
      
      if avgExecTime > maxExec:
        maxExec = avgExecTime
      iIndex += 1
    nextRegionYIndex[label] += 1
  tIndex += 1
          
colors = ['C0o','C1o','C2o','C3o','C4o','C5o','C6o','C7o','C8o','C9o']
for i in range(0, len(execTimeX)):
  plt.plot(execTimeX[i], regionY[i], colors[i], label=instanceLabels[i], ms=3)

# doted line between regions
for i in range(0, len(regionLabels) - 1):
  x = regionPrefixSum[regionLabels[i]] + regionCounts[regionLabels[i]] + i
  plt.plot([0, maxExec + 100], [x, x], 'C7--', linewidth=1)

plt.xlabel("Avg execution time in s")
plt.ylabel("Region")
plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)

# y ticks for regions
yTick = []
for i in range(0, len(regionLabels)):
  yTick.append(regionPrefixSum[regionLabels[i]] + regionCounts[regionLabels[i]] / 2 + i)
    
plt.yticks(yTick, regionLabels)
plt.axis([0, maxExec + 100, -1, curPrefixSum + len(regionLabels)])
plt.savefig('exec_time.png')
plt.show()
