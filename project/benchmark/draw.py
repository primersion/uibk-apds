#!/usr/bin/python3

import matplotlib.pyplot as plt
import numpy as np
import csv
import sys

def gen_graph(inputFile, outputFile):

	values = []
	with open(inputFile, 'r') as csvfile:
		csvReader = csv.reader(csvfile, delimiter=',')
		for row in csvReader:
			values.append(float(row[0]))

	x = np.arange(0, len(values) / 4, 0.25)

	plt.figure(figsize=(10,8))
	plt.plot(x, values, label='CPU Usage')
	plt.xlabel('Time in seconds')
	plt.ylabel('CPU Usage in percent')
	plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)

	plt.savefig(outputFile)
	plt.show()


if len(sys.argv) < 3:
	print('Usage: %s <inputFile> <outputFile>' % sys.argv[0])
	sys.exit(1)

gen_graph(sys.argv[1], sys.argv[2])
