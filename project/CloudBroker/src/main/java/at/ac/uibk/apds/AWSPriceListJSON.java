package at.ac.uibk.apds;
// needs JSON-library from here: http://www.java2s.com/Code/Jar/j/Downloadjavajsonjar.htm 

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class loads the current offer-file for Amazon EC2 and extracts the
 * price-per-hour for some specific instance-types in some specific regions
 */
public class AWSPriceListJSON {

	private static AWSPriceListJSON instance = null;
	private TreeMap<String, Machine> machinesTM;
	private ArrayList<Machine> machines;
	private String version;
	private String publicationDate;
	private String publicationTime;
	private final String offerIndexAddress = "https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/index.json";
	private final String offerFileAddress = "https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/AmazonEC2/current/index.json";
	@SuppressWarnings("unused")
	private final String offerFileTestAddress = "https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/AmazonEC2/current/eu-central-1/index.json";
	private final Path PRICE_LIST_FOLER = Paths.get(System.getProperty("user.home"), "priceList");
	private BigInteger newestVersion;

	private AWSPriceListJSON() {
		this.machinesTM = new TreeMap<String, Machine>();
		this.machines = new ArrayList<Machine>();
		loadPriceList();
	}

	public static AWSPriceListJSON getInstance() {
		if (instance == null) {
			instance = new AWSPriceListJSON();
		}
		return instance;
	}

	public double getPriceRegionInstanceType(String regionCode, String instanceType) {
		String key = regionCode + instanceType;
		Machine m = machinesTM.get(key);
		return m.getPricePerHour();
	}

	private void loadPriceList() {
		System.out.println("Loading prices.");
		File priceListOnDisk = checkPriceListsOnDisk();
		try {
			if (priceListOnDisk == null) {
				File newPriceList = downloadFileToDisk(new URL(offerFileAddress));
				loadPricesFromOfferFile(newPriceList);
			} else {
				System.out.println("Loading pricelist from disk.");
				loadPricesFromOfferFile(priceListOnDisk);
			}
		} catch (MalformedURLException error) {
			error.printStackTrace();
		}
	}

	private File downloadFileToDisk(URL url) {
		System.out.println("Downloading new pricelist.");
		Path path = null;
		try (InputStream in = url.openStream()) {
			path = Paths.get(PRICE_LIST_FOLER.toString(), this.newestVersion.toString());
			Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return path.toFile();
	}

	/**
	 * Checks downloaded pricelists and compares with online-version
	 * 
	 * @return most current pricelist
	 */
	private File checkPriceListsOnDisk() {
		File pricesFolderFile = PRICE_LIST_FOLER.toFile();
		File[] pricesOnDiskArray;
		File pricesNewestOnDiskFile = null;
		BigInteger latestOnlineVersionBI = new BigInteger("0");
		BigInteger fileNewestOnDiskBI = new BigInteger("0");

		if (!pricesFolderFile.exists()) {
			pricesFolderFile.mkdirs();
		}
		pricesOnDiskArray = pricesFolderFile.listFiles();
		latestOnlineVersionBI = getOnlineVersion();
		if (pricesOnDiskArray.length <= 0) {
			System.out.println("No pricelists found on disk.");
			pricesNewestOnDiskFile = null;
			this.newestVersion = latestOnlineVersionBI;
		} else {
			for (File file : pricesOnDiskArray) {
				BigInteger fileNameBI = new BigInteger(file.getName());
				if (pricesNewestOnDiskFile == null) {
					pricesNewestOnDiskFile = file;
					fileNewestOnDiskBI = fileNameBI;
				} else {
					if (fileNameBI.compareTo(fileNewestOnDiskBI) > 0) {
						pricesNewestOnDiskFile = file;
						fileNewestOnDiskBI = fileNameBI;
					}
				}
			}
			if (latestOnlineVersionBI.compareTo(fileNewestOnDiskBI) <= 0) {
				return pricesNewestOnDiskFile;
			} else {
				this.newestVersion = latestOnlineVersionBI;
				System.out.println("Pricelist online newer than pricelists on disk.");
			}
		}
		return null;
	}

	private BigInteger getOnlineVersion() {
		BigInteger version = new BigInteger("0");
		try {
			JSONObject indexFile = new JSONObject(readFileFromUrl(new URL(offerIndexAddress)));
			String publicationDate = indexFile.getString("publicationDate");
			// "publicationDate" : "2017-05-26T18:19:56Z"
			publicationDate = publicationDate.replace("-", "");
			publicationDate = publicationDate.replace("T", "");
			publicationDate = publicationDate.replace(":", "");
			publicationDate = publicationDate.replace("Z", "");
			version = new BigInteger(publicationDate);
		} catch (MalformedURLException | JSONException error) {
			error.printStackTrace();
		}
		return version;
	}

	/**
	 * Processes the offer-file
	 */
	private void loadPricesFromOfferFile(File file) {

		JSONObject offerFile = null;

		try {
			offerFile = new JSONObject(readFileFromDisk(file));
		} catch (JSONException e) {
			e.printStackTrace();
		}

		this.setVersion(offerFile.getString("version"));
		this.setPublicationDate(offerFile.getString("publicationDate"));

		JSONObject products = (JSONObject) offerFile.get("products");
		JSONObject terms = (JSONObject) ((JSONObject) offerFile.get("terms")).get("OnDemand");
		JSONArray product_names = products.names();

		String offerTermCode = "JRTCKXETXF"; // term: hourly
		String rateCode = "6YS6EN2CT7"; //

		Map<String, Void> regions = RegionUtils.getSelectedRegionNamesAsMap();
		Map<String, Void> instanceTypes = InstanceUtils.getInstanceTypesAsMap();
		Map<String, String> regionCodes = RegionUtils.getSelectedRegionsWithCodeAsMap();
		for (int i = 0; i < product_names.length(); i++) {

			String sku = product_names.get(i).toString();
			JSONObject attributes = (JSONObject) ((JSONObject) products.get(sku)).get("attributes");
			String location = null;
			String instanceType = null;
			String operatingSystem = null;
			String tenancy = null;

			try {
				location = attributes.getString("location");
				instanceType = attributes.getString("instanceType");
				operatingSystem = attributes.getString("operatingSystem");
				tenancy = attributes.getString("tenancy");
			} catch (JSONException error) {
				continue;
			}

			if (operatingSystem.equals("Linux") && tenancy.equals("Shared") && regions.containsKey(location)
					&& instanceTypes.containsKey(instanceType)) {
				JSONObject productTerms = (JSONObject) terms.get(sku);
				JSONObject pricePerUnit = (JSONObject) ((JSONObject) ((JSONObject) ((JSONObject) productTerms
						.get(sku + "." + offerTermCode)).get("priceDimensions"))
								.get(sku + "." + offerTermCode + "." + rateCode)).get("pricePerUnit");
				String key = regionCodes.get(location) + instanceType;
				Machine newMachine = new Machine(regionCodes.get(location), instanceType,
						pricePerUnit.getDouble("USD"));
				this.machinesTM.put(key, newMachine);				
				this.machines.add(newMachine);
			}
		}
		this.machines.sort((m1, m2) -> m1.getRegion().compareTo(m2.getRegion()));
	}

	public ArrayList<Machine> getMachines() {
		ArrayList<Machine> machines = new ArrayList<>();
		for (Map.Entry<String, Machine> entry : this.machinesTM.entrySet()) {
			machines.add(entry.getValue());
		}
		return machines;
	}

	private String readFileFromDisk(File file) {

		String content = null;
		try (BufferedReader br = new BufferedReader(new FileReader(file.getPath()))) {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			content = sb.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content;
	}

	/**
	 * Reads file from URL
	 * 
	 * @return returns the file as String
	 */
	private String readFileFromUrl(URL address) {

		BufferedReader reader = null;
		int chars_read = 0;
		StringBuffer buffer = new StringBuffer();

		try {
			reader = new BufferedReader(new InputStreamReader(address.openStream()));
			char[] chars = new char[1024];
			while ((chars_read = reader.read(chars)) != -1) {
				buffer.append(chars, 0, chars_read);
			}
			return buffer.toString();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return buffer.toString();
	}

	public void printFullBenchmarkCost() {
		System.out.println("");
		System.out.format("AWS Pricelist   version: %s\n", version);
		System.out.format("                   date: %s\n", publicationDate);
		System.out.format("                   time: %s\n", publicationTime);
		System.out.println();
		System.out.format("amount selected regions: %d\n", RegionUtils.getSelectedRegionCodes().length);
		System.out.format("         instance-types: %d\n", InstanceUtils.getInstanceTypes().length);
		System.out.println();
		System.out.format("Price full benchmark : %.2f USD/h\n", getPriceFullBenchmark());
	}

	public double getPriceFullBenchmark() {
		double sum = 0.0;
		for (Map.Entry<String, Machine> m : machinesTM.entrySet()) {
			sum += m.getValue().getPricePerHour();
		}
		return sum;
	}

	public TreeMap<String, Machine> getEC2Prices() {
		return this.machinesTM;
	}

	public String getVersion() {
		return this.version;
	}

	private void setVersion(String version) {
		this.version = version;
	}

	public String getPublicationDate() {
		return publicationDate;
	}

	private void setPublicationDate(String publicationDate) {
		String[] values = publicationDate.split("[TZ]");
		this.publicationDate = values[0];
		this.publicationTime = values[1];
	}
	
	public void printAsCSV(){
		for(Map.Entry<String, Machine> m : machinesTM.entrySet()){
			System.out.println(m.getValue().toCSVString());
		}
	}

	public static void main(String[] args) {

		AWSPriceListJSON priceList = AWSPriceListJSON.getInstance();
//		priceList.printFullBenchmarkCost();
		priceList.printAsCSV();
	}

}
