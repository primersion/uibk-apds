package at.ac.uibk.apds;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class S3Utils {

	public static final String S3_LOG_BUCKET_NAME = "brockerlogbucket" + System.getProperty("user.name");
	public static final String S3_LOCAL_DOWNLOAD_FOLDER = System.getProperty("user.home") + "/log/";
	public static final int BENCHMARK_ITERATIONS = 5; //If changed also update run.sh

	static void deleteS3Folder(AmazonS3 s3, String bucketName, String folderPath) {
		for (S3ObjectSummary file : s3.listObjects(bucketName, folderPath).getObjectSummaries()) {
			s3.deleteObject(bucketName, file.getKey());
		}
	}

	static void createBucket(AmazonS3 s3, String bucketName) {
		boolean bucketExists = false;

		/**************** Check if bucket already exists **************/
		for (Bucket bucket : s3.listBuckets()) {
			if (bucket.getName().equals(bucketName)) {
				bucketExists = true;
				System.out.println("Bucket " + bucket.getName() + " already exists");
				break;
			}
		}

		/****************
		 * Create bucket if it doesn't already exist
		 **************/
		if (!bucketExists) {
			System.out.println("Creating bucket " + bucketName);
			s3.createBucket(bucketName);
		}
	}

	static void downloadS3Folder(AmazonS3 s3, String bucketName, String srcS3Path, String dstPath) {
		for (S3ObjectSummary file : s3.listObjects(bucketName, srcS3Path).getObjectSummaries()) {
			String filePath = file.getKey();
			int start = filePath.indexOf('/');
			filePath = filePath.substring(start + 1, filePath.length() - 1);
			Path path = new File(dstPath + filePath).toPath();
			
			if(Files.exists(path)) {
				continue;
			}
			
			S3Object object = s3.getObject(new GetObjectRequest(bucketName, file.getKey()));

			try {

				Files.createDirectories(path.getParent());
				Files.copy(object.getObjectContent(), path);
				object.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}
	
	static void waitBenchmarkCompletion(AmazonS3 s3, String bucketName, String srcS3Path, boolean test) {
		System.out.println("Waiting for benchmark results...");
		ArrayList<S3ObjectSummary> availFiles;
		String status;
		
		int regionCount = RegionUtils.getSelectedRegions().length;
		int instanceCount;
		
		if(test) {
			instanceCount = InstanceUtils.getTestInstanceType().length;
		}
		else {
			instanceCount = InstanceUtils.getInstanceTypes().length;
		}
		
		int totalFileCount = regionCount * instanceCount * BENCHMARK_ITERATIONS;
		
		while(true) {
			availFiles = (ArrayList<S3ObjectSummary>) s3.listObjects(bucketName, srcS3Path).getObjectSummaries();
			status = String.format("%1$03d out of %2$03d benchmarks completed.", availFiles.size(), totalFileCount);
			System.out.println(status);
			
			if(availFiles.size() == totalFileCount) {
				System.out.println("All benchmarks finished.");
				break;
			}
			else {
				try {
					Thread.sleep(15000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

}
