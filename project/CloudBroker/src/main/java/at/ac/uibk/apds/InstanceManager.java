package at.ac.uibk.apds;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.codec.binary.Base64;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.AuthorizeSecurityGroupIngressRequest;
import com.amazonaws.services.ec2.model.CreateKeyPairRequest;
import com.amazonaws.services.ec2.model.CreateKeyPairResult;
import com.amazonaws.services.ec2.model.CreateSecurityGroupRequest;
import com.amazonaws.services.ec2.model.CreateSecurityGroupResult;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.IamInstanceProfileSpecification;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.IpPermission;
import com.amazonaws.services.ec2.model.IpRange;
import com.amazonaws.services.ec2.model.KeyPair;
import com.amazonaws.services.ec2.model.KeyPairInfo;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.SecurityGroup;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;

public class InstanceManager {
	
	public static final String CLOUD_BROKER_SECURITY_GROUP = "CloudBrokerSecurityGroup";
	public static final String CLOUD_BROKER_KEY_PAIR_NAME = "CloudBrokerKeyPair";
	public static final String KEY_DIR = "/home/" + System.getProperty("user.name") + "/keys/";
	public static final int[] INSTANCE_PORTS = new int[]{22};
	public static final String FREE_INSTANCE_TYPE = "t2.micro";
	
	private Map<Regions, AmazonEC2> ec2;
	private AmazonIdentityManagement iam;
	private Map<Regions, String> amis;
	private Map<Regions, List<Instance>> runningInstances;
	private IamInstanceProfileSpecification iamRole;
	
	private Regions[] regions;
	private String[] instanceTypes;
	
	public InstanceManager(boolean testModeEnabled){
		if(testModeEnabled)
			instanceTypes = InstanceUtils.getTestInstanceType();
		else
			instanceTypes = InstanceUtils.getInstanceTypes();
		regions = RegionUtils.getSelectedRegions();
		
		System.out.println("Creating EC2 clients");
		ec2 = EC2Utils.createEC2forRegions(regions);
		
		System.out.println("Creating Amazon identity manager");
		iam = AmazonIdentityManagementClientBuilder.standard()
	            									 .withRegion(Regions.EU_CENTRAL_1)
	            									 .build();
		
		iamRole = new IamInstanceProfileSpecification()
				  .withArn(IAMUtils.getS3AccessIAMProfile(iam).getArn());
		
		System.out.println("Getting AMIS of all regions");
		amis = AMIFiltering.getAMISForAllRegions(ec2);
		runningInstances = new HashMap<>();
	}
	
	public void startAll(){
		System.out.println("Starting all instances");
		String userData;
		
		for(Entry<Regions, AmazonEC2> e : ec2.entrySet()){
			System.out.println("Creating instances for region " + e.getKey());
			AmazonEC2 ec2R = e.getValue();
			String groupId = checkSecurityGroup(ec2R);
			String keyPairName = checkKeyPair(ec2R, e.getKey());
				
			// start instance
			for(String s : instanceTypes){
				userData = getECuserData(S3Utils.S3_LOG_BUCKET_NAME, e.getKey().getName(), s);
				List<Instance> instances = startInstance(ec2R, e.getKey(), s, keyPairName, groupId, userData);

				List<String> instanceIds = new LinkedList<String>();
				for (Instance ins : instances) {
					instanceIds.add(ins.getInstanceId());
					ec2R.createTags(tagInstance(ins));
					System.out.println("New instance has been created with " + s + ": " + ins.getInstanceId());
				}
				
				if(runningInstances.containsKey(e.getKey()))
					runningInstances.get(e.getKey()).addAll(instances);
				else
					runningInstances.put(e.getKey(), instances);
			}
		}
		
		for(Entry<Regions, List<Instance>> e : runningInstances.entrySet())
			runningInstances.put(e.getKey(), InstanceUtils.waitUntilInstancesRunning(ec2.get(e.getKey()), 2500, e.getValue()));
		System.out.println("All instances started");
	}
	
	public void startFromVector(InstanceVector instanceVec){
		System.out.println("Starting requested instance");
		Regions region = Regions.fromName(instanceVec.getRegion());
		String instanceType = instanceVec.getInstanceType();
		
		AmazonEC2 ec2R = ec2.get(region);
		String groupId = checkSecurityGroup(ec2R);
		String keyPairName = checkKeyPair(ec2R, region);
		
		List<Instance> instances = startInstance(ec2R, region, instanceType, keyPairName, groupId, "");
		
		List<String> instanceIds = new LinkedList<String>();
		for (Instance ins : instances) {
			instanceIds.add(ins.getInstanceId());
			ec2R.createTags(tagInstance(ins));
			System.out.println("New instance has been created with " + instanceType + ": " + ins.getInstanceId()  + " region: " + region.getName());
		}
		
		if(runningInstances.containsKey(region))
			runningInstances.get(region).addAll(instances);
		else
			runningInstances.put(region, instances);
	}
	
	private CreateTagsRequest tagInstance(Instance instance) {
		System.out.println("Setting machine tag");
		ArrayList<Tag> machineTags = new ArrayList<Tag>();
		machineTags.add(new Tag("Usage", "Cloudbroker-Benchmark"));
		CreateTagsRequest machineTagReq = new CreateTagsRequest().withTags(machineTags)
				.withResources(instance.getInstanceId());
		return machineTagReq;
		
	}
	
	private List<Instance> startInstance(AmazonEC2 ec2, Regions region, String instanceType, String keyPairName, String groupId, String userData){
		int mIC = 1;
		int maxIC = 1;
		
		RunInstancesRequest rir = new RunInstancesRequest(amis.get(region), mIC, maxIC);
		rir.setInstanceType(instanceType);
		rir.setKeyName(keyPairName);
		rir.withSecurityGroupIds(groupId);
		rir.setIamInstanceProfile(iamRole);
		if(userData != null)
			rir.setUserData(userData);
		
		RunInstancesResult result = ec2.runInstances(rir);
		return result.getReservation().getInstances();
	}
	
	private String getECuserData(String bucketName, String region, String instanceName) {
	    String userData = "";
	    userData += "#!/bin/bash" + "\n";
	    userData += "sudo yum -y update\n";
	    userData += "sudo yum -y install dstat java-1.8.0-openjdk java-1.8.0-openjdk-devel git\n";
	    userData += "sudo update-alternatives --set java /usr/lib/jvm/jre-1.8.0-openjdk.x86_64/bin/java\n";
	    userData += "git clone https://gitlab.com/primersion/cloud-broker-benchmark.git /home/ec2-user/cloud-broker-benchmark\n";
	    userData += "sudo yum -y install cmake gcc-c++ openmpi openmpi-devel\n";
	    userData += "sudo ln -s /usr/lib64/openmpi/bin/mpirun /usr/bin/mpirun\n";
	    userData += "sudo ln -s /usr/lib64/openmpi/bin/mpicc /usr/bin/mpicc\n";
	    userData += "sudo ln -s /usr/lib64/openmpi/bin/mpicxx /usr/bin/mpicxx\n";
	    userData += "cd /home/ec2-user/cloud-broker-benchmark\n";
	    userData += "sudo ./run.sh " + bucketName + " " + region + " " + instanceName;
	    
//	    userData += "javac MatrixMul.java\n";
//	    userData += "java MatrixMul\n";
	        
//	    REQUIREMENTS FOR MPI
//	    sudo yum -y install cmake gcc-c++ openmpi openmpi-devel
//	    sudo ln -s /usr/lib64/openmpi/bin/mpirun /usr/bin/mpirun
//	    sudo ln -s /usr/lib64/openmpi/bin/mpicc /usr/bin/mpicc
//	    sudo ln -s /usr/lib64/openmpi/bin/mpicxx /usr/bin/mpicxx
	    
//	    REQUIREMENTS FOR GLANCES
//	    gcc
//	    sudo yum install python35 python35-devel python35-pip 
//	    sudo pip install -U pip setuptools 
//	    sudo python3 -m pip install glances
	    
	    return encodeBase64(userData);
	}
	
	public String encodeBase64(String input){
		String base64UserData = null;
	    try {
	        base64UserData = new String( Base64.encodeBase64(input.getBytes( "UTF-8" )), "UTF-8" );
	    } catch (UnsupportedEncodingException e) {
	        e.printStackTrace();
	    }
	    return base64UserData;
	}
	
	private String checkKeyPair(AmazonEC2 ec2, Regions region){
		String keyName = null;
		List<KeyPairInfo> keyPairs = EC2Utils.getKeyPairs(ec2);
		for(KeyPairInfo k : keyPairs){
			if(k != null && k.getKeyName().equals(CLOUD_BROKER_KEY_PAIR_NAME)){
				System.out.println("Using existing keypair " + k.getKeyName());
				keyName = k.getKeyName();
				break;
			}
		}
		
		if(keyName == null)
			keyName = createKeyPair(ec2, region);
		return keyName;
	}
	
	private String createKeyPair(AmazonEC2 ec2, Regions region) {
		/***************** Create a key for the VM ****************/
		CreateKeyPairRequest newKReq = new CreateKeyPairRequest();
		newKReq.setKeyName(CLOUD_BROKER_KEY_PAIR_NAME);
		CreateKeyPairResult kresult = ec2.createKeyPair(newKReq);
		KeyPair keyPair = kresult.getKeyPair();
		System.out.println("Key for the VM was created  = " + keyPair.getKeyName() + "\nthe fingerprint is="
				+ keyPair.getKeyFingerprint() + "\nthe material is= \n" + keyPair.getKeyMaterial());

		/***************** Store the key in a .pem file ****************/
		String keyName = keyPair.getKeyName();
		String fileName = KEY_DIR + region.toString() + keyName + ".pem";
		File distFile = new File(fileName);
		
		try{
			BufferedReader bufferedReader = new BufferedReader(new StringReader(keyPair.getKeyMaterial()));
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(distFile));
			char buf[] = new char[1024];
			int len;
			while ((len = bufferedReader.read(buf)) != -1) {
				bufferedWriter.write(buf, 0, len);
			}
			bufferedWriter.flush();
			bufferedReader.close();
			bufferedWriter.close();
		} catch(IOException e){
			System.out.println("Failed writing keypair to file");
		}
		
		return keyName; 
	}
	
	private String checkSecurityGroup(AmazonEC2 ec2){
		List<SecurityGroup> groups = EC2Utils.getSecurityGroups(ec2);
		
		String gid = null;
		for(SecurityGroup g : groups){
			if(g != null && g.getGroupName().equals(CLOUD_BROKER_SECURITY_GROUP)){
				gid = g.getGroupId();
				System.out.println("Using existing security group " + g.getGroupName());
				break;
			}
		}
		
		if(gid == null){
			System.out.println("Creating new security group");
			gid = createSecurityGroup(ec2, INSTANCE_PORTS);
		}
		return gid;
	}
	
	private String createSecurityGroup(AmazonEC2 ec2, int[] ports){
		try {
			CreateSecurityGroupRequest csgr = new CreateSecurityGroupRequest();
			csgr.withGroupName(CLOUD_BROKER_SECURITY_GROUP).withDescription("Security group for cloud broker application");
			CreateSecurityGroupResult resultsc = ec2.createSecurityGroup(csgr);
			System.out.println(String.format("Security group created: [%s]", resultsc.getGroupId()));
			
			String groupId = resultsc.getGroupId();
			setTrafficPolicy(ec2, groupId, ports);
			
			return groupId;
		} catch (AmazonServiceException ase) {
			System.out.println(ase.getMessage());
		}
		return null;
	}
	
	private void setTrafficPolicy(AmazonEC2 ec2, String groupId, int[] ports){
		/***************** Set incoming traffic policy ****************/
		
		String ipAddress;
		try {
			ipAddress = NetworkUtils.getMyIp();
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		System.out.println("My IP: " + ipAddress);

		IpRange ipRange = new IpRange().withCidrIp(ipAddress + "/32");
		
		List<IpPermission> permissions = new ArrayList<>();
		for(int i : ports){
			IpPermission ipPermission = new IpPermission();
			ipPermission.withIpv4Ranges(Arrays.asList(new IpRange[] { ipRange })).withIpProtocol("tcp").withFromPort(i)
					.withToPort(i);
			permissions.add(ipPermission);
		}

		/***************** Authorize ingress traffic ****************/
		try {
			AuthorizeSecurityGroupIngressRequest ingressRequest = new AuthorizeSecurityGroupIngressRequest();

			ingressRequest.withGroupId(groupId).withIpPermissions(permissions);
			ec2.authorizeSecurityGroupIngress(ingressRequest);
			System.out.println("Finished setting traffic policy");
		} catch (AmazonServiceException ase) {
			System.out.println(ase.getMessage());
		}
	}
	
	public void terminateAll(){
		for(Entry<Regions, List<Instance>> e : runningInstances.entrySet()){
			InstanceUtils.terminateInstances(ec2.get(e.getKey()), e.getValue());
		}
	}
	
	public static void main(String[] args){
		InstanceManager im = new InstanceManager(true);
		im.startAll();
		
		try {
			Thread.sleep(600000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		im.terminateAll();
	}
}
