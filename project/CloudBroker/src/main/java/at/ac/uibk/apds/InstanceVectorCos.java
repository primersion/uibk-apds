package at.ac.uibk.apds;

public class InstanceVectorCos extends InstanceVector {

	
	public InstanceVectorCos(String r, String i, double[] v) {
		super(r, i, v);
		
		double[] vNew = new double[v.length];
		for (int idx = 0;idx<v.length;idx++) {
			vNew[idx] = v[idx] * 1.0;
		}
		this.setValues(vNew);
	}
	
	public InstanceVectorCos(String r, String i, double[] v, double[] w) {
		super(r, i, v);

		double[] vNew = new double[v.length];
		for (int idx = 0;idx<v.length;idx++) {
			vNew[idx] = v[idx] * w[idx];
		}
		this.setValues(vNew);
	}

	@Override
	public void calculateSimilarity(InstanceVector ideal, InstanceVector antiIdeal) {
		double distanceIdeal = this.calculateSimilarity(ideal);
		double distanceAntiIdeal = this.calculateSimilarity(antiIdeal);
		this.setSimilarity(1 - (distanceAntiIdeal / (distanceIdeal + distanceAntiIdeal)));
	}
}
