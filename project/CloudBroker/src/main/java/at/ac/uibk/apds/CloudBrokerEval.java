package at.ac.uibk.apds;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

public class CloudBrokerEval {

	public static final String BENCHMARK_EVAL_RESULT_FOLDER = "/home/alexander/git/uibk-apds/project/benchmark/results_uibk/";
	public static final String PING_EVAL_RESULT_FOLDER = "/home/alexander/git/uibk-apds/project/ping/results_uibk/";
	public static final String PING_EVAL_RESULT_FILENAME = "pings_2017-06-07_01-06-10.txt";
	public static final String RANKING_FOLDER = "/home/alexander/git/uibk-apds/project/evaluation/ranking/";
	
	public static double[] TEST_USER_VECTOR;
	
	public static final int MODEL_COS = 0;
	public static final int MODEL_TOPSIS_COS = 1;
	public static final int MODEL_TOPSIS_L2 = 2;
	
	public static final String[] MODEL_NAMES = new String[]{"cos", "topsis_cos", "topsis_l2"};
	
	public static void main(String[] args){
		File benchData = new File(BENCHMARK_EVAL_RESULT_FOLDER);
		File[] timestamps = benchData.listFiles();
//		for(int i = 0; i < timestamps.length; i++){
//			if(timestamps[i].isFile()){
//				System.out.println("File: " + timestamps[i].getName());
//			} else{
//				System.out.println("Dir: " + timestamps[i].getName());
//			}
//		}
		
		AWSPriceListJSON priceList = AWSPriceListJSON.getInstance();
		
		Map<String, String> pingRegions = RegionUtils.getPingRegions();
		String[][] pings = new String[pingRegions.keySet().size()][2];
		
		for(int k = 0; k < timestamps.length; k++){
			for(int m = 0; m < 3; m++){
				TEST_USER_VECTOR = new double[]{10, 5, 5, 1, 5};
				
				StringBuilder filenameBuilder = new StringBuilder();
	//			filenameBuilder.append(timestamps[k].getName() + "_");
				for(int i = 0; i < TEST_USER_VECTOR.length; i++){
					filenameBuilder.append(TEST_USER_VECTOR[i]);
					if(i < TEST_USER_VECTOR.length - 1)
						filenameBuilder.append("_");
				}
	//			filenameBuilder.append(".csv");
				
				try {
					String folder = RANKING_FOLDER + filenameBuilder.toString() + "/" + MODEL_NAMES[m] + "/";
					File outputFolder = new File(folder);
					if(!outputFolder.exists())
						outputFolder.mkdirs();
					PrintWriter out = new PrintWriter(folder + timestamps[k].getName() + ".csv");
				
					int nextIndex = 0;
					File pingData = new File(PING_EVAL_RESULT_FOLDER);
					File[] regions = pingData.listFiles();
					for(int i = 0; i < regions.length; i++){
						if(!regions[i].isFile()){
							double ping = getPingFromCSV(PING_EVAL_RESULT_FOLDER + regions[i].getName() + "/" + PING_EVAL_RESULT_FILENAME, timestamps[k].getName());
							pings[nextIndex][0] = ping + "";
							pings[nextIndex][1] = pingRegions.get(regions[i].getName().replace("_",	"-"));
							nextIndex++;
						}
					}
					
					
					// Rate all instances
					System.out.println("Calculating instance similarities...");
					InstanceFinder vs = new InstanceFinder(BenchmarkUtils.readLogFolder(BENCHMARK_EVAL_RESULT_FOLDER + timestamps[k].getName() + "/"), priceList, pings);
					
					if(m == MODEL_COS)
						vs.rateInstances(TEST_USER_VECTOR);
					else if(m == MODEL_TOPSIS_L2)
						vs.rateInstancesTopsisL2(TEST_USER_VECTOR);
					else
						vs.rateInstancesTopsisCos(TEST_USER_VECTOR);
			
					String queryString = "-1," + vs.getQueryVector().toCSVString();
	//				System.out.println(queryString);
					if(m == MODEL_COS)
						out.println(queryString);
					else{
						String idealString = "-1," + vs.getIdealVector().toCSVString();
						String antiIdealString = "-1," + vs.getAntiIdealVector().toCSVString();
						
						out.println(idealString);
						out.println(antiIdealString);
					}
					
					int instanceId = 0;
					String sInstance;
					for (InstanceVector instance : vs.getDecMat()) {
			//			sInstance = String.format("%1$03d: %2$s", instanceId, instance);
						sInstance = String.format("%1$d,%2$s", instanceId, instance.toCSVString());
	//					System.out.println(sInstance);
						out.println(sInstance);
						instanceId++;
					}
				
					out.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
			
	}
	
	public static double getPingFromCSV(String csvFile, String timestamp) {
		BufferedReader br;

		try {
			br = new BufferedReader(new FileReader(csvFile));
			String line;

			double avgPing = 0;
			double avgPingFullHour = 0;

			while ((line = br.readLine()) != null && !line.isEmpty()) {
				String[] fields = line.split(" ");
				String pingTimestamp = fields[0];
				if(fields.length > 4){
					if(pingTimestamp.startsWith(timestamp.substring(0, 16)))
						avgPing = Double.valueOf(fields[4].split("/")[1]);
					if(pingTimestamp.startsWith(timestamp.substring(0, 13)))
						avgPingFullHour = Double.valueOf(fields[4].split("/")[1]);;
				}
			}

			br.close();

			if(avgPing != 0)
				return avgPing;
			if(avgPingFullHour != 0)
				return avgPingFullHour;

		} catch (IOException e) {
			e.printStackTrace();
		}

		return -1;
	}
}
