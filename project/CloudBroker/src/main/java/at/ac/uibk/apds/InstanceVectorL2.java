package at.ac.uibk.apds;

public class InstanceVectorL2 extends InstanceVector {

	public InstanceVectorL2(String r, String i, double[] v) {
		super(r, i, v);
		
		double[] vNew = new double[v.length];
		for (int idx = 0;idx<v.length;idx++) {
			vNew[idx] = v[idx] * 1.0;
		}
		this.setValues(vNew);
	}
	
	public InstanceVectorL2(String r, String i, double[] v, double[] w) {
		super(r, i, v);

		double[] vNew = new double[v.length];
		for (int idx = 0;idx<v.length;idx++) {
			vNew[idx] = v[idx] * w[idx];
		}
		this.setValues(vNew);
	}

	@Override
	public void calculateSimilarity(InstanceVector ideal, InstanceVector antiIdeal) {
		double distanceIdeal = this.calculateL2Distance(ideal);
		double distanceAntiIdeal = this.calculateL2Distance(antiIdeal);
		this.setSimilarity(distanceAntiIdeal / (distanceIdeal + distanceAntiIdeal));
	}

	@Override
	public int compareTo(InstanceVector o) {
		if (getSimilarity() < o.getSimilarity()) {
			return 1;
		} else if (getSimilarity() > o.getSimilarity()) {
			return -1;
		}
		return 0;
	}
}
