package at.ac.uibk.apds;

import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.model.AddRoleToInstanceProfileRequest;
import com.amazonaws.services.identitymanagement.model.CreateInstanceProfileRequest;
import com.amazonaws.services.identitymanagement.model.CreateInstanceProfileResult;
import com.amazonaws.services.identitymanagement.model.CreateRoleRequest;
import com.amazonaws.services.identitymanagement.model.CreateRoleResult;
import com.amazonaws.services.identitymanagement.model.GetInstanceProfileRequest;
import com.amazonaws.services.identitymanagement.model.GetInstanceProfileResult;
import com.amazonaws.services.identitymanagement.model.InstanceProfile;
import com.amazonaws.services.identitymanagement.model.NoSuchEntityException;
import com.amazonaws.services.identitymanagement.model.PutRolePolicyRequest;
import com.amazonaws.services.identitymanagement.model.Role;
import com.amazonaws.services.identitymanagement.model.ServiceFailureException;

public class IAMUtils {
	public static final String S3_ACCESS_IAM_ROLE = "S3FullAccessIAM";
	
	private static final String S3_ACCESS_POLICY = 
            "{" + 
                "\"Version\": \"2012-10-17\"," + 
                "\"Statement\": [" + 
                    "{" + 
                        "\"Effect\": \"Allow\"," + 
                        "\"Action\": \"s3:*\"," + 
                        "\"Resource\": \"*\"" + 
                    "}" + 
                "]" + 
            "}";
 
	private static final String ASSUME_ROLE_POLICY = 
            "{" + 
                "\"Version\": \"2012-10-17\"," + 
                "\"Statement\": [" + 
                    "{" + 
                        "\"Sid\": \"\"," + 
                        "\"Effect\": \"Allow\"," + 
                        "\"Principal\": {" + 
                            "\"Service\": \"ec2.amazonaws.com\"" + 
                        "}," + 
                        "\"Action\": \"sts:AssumeRole\"" + 
                    "}" + 
                "]" + 
            "}"; 
	
	public static InstanceProfile getS3AccessIAMProfile(AmazonIdentityManagement iam) {	
		
		/***************** Check if profile already exists ****************/
		try {
			  GetInstanceProfileRequest gipr = new GetInstanceProfileRequest()
					  					 	   .withInstanceProfileName(S3_ACCESS_IAM_ROLE);
			  GetInstanceProfileResult result = iam.getInstanceProfile(gipr);
			  InstanceProfile profile = result.getInstanceProfile();
			  
			  System.out.println("Using existing S3 Access IAM instance profile");
			  return profile;
			  
		}
		catch(NoSuchEntityException e) {
			//Ignore and create new profile
		}
		catch (ServiceFailureException e) {
			System.out.println(e.getMessage());
		}
		
		/***************** Create new IAM role ****************/
		System.out.println("Creating new S3 Access IAM role and instance profile");
		
		CreateRoleRequest rr = new CreateRoleRequest()
								   .withRoleName(S3_ACCESS_IAM_ROLE)
								   .withAssumeRolePolicyDocument(ASSUME_ROLE_POLICY);
		
		CreateRoleResult result = iam.createRole(rr);
		Role role = result.getRole();
		
		PutRolePolicyRequest rpr = new PutRolePolicyRequest()
				.withRoleName(role.getRoleName())
				.withPolicyName("AmazonS3FullAccess")
				.withPolicyDocument(S3_ACCESS_POLICY);
		iam.putRolePolicy(rpr);
		
		/***************** Create new instance profile ****************/
		CreateInstanceProfileRequest ipr = new CreateInstanceProfileRequest()
										   .withInstanceProfileName(S3_ACCESS_IAM_ROLE);
		CreateInstanceProfileResult profileResult = iam.createInstanceProfile(ipr);
		InstanceProfile profile = profileResult.getInstanceProfile();
		
		AddRoleToInstanceProfileRequest ripr = new AddRoleToInstanceProfileRequest()
											   .withInstanceProfileName(profile.getInstanceProfileName())
											   .withRoleName(role.getRoleName());
		iam.addRoleToInstanceProfile(ripr);
		
		return profile;
	}

}
