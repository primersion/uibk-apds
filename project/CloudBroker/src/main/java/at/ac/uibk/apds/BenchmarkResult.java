package at.ac.uibk.apds;

import com.amazonaws.regions.Regions;

//Class representing the result of one benchmark evaluation
public class BenchmarkResult {
	private Regions region;
	private String instanceType;
	private int numProc;
	private int problemSize;
	
	private double execTime;
	private double avgCpuUsage;
	private double avgUsedMemory;
	private double avgFreeMemory;
	
	public BenchmarkResult(Regions region, String instanceType, int numProc, int problemSize) {
		super();
		this.region = region;
		this.instanceType = instanceType;
		this.numProc = numProc;
		this.problemSize = problemSize;
	}
	
	public BenchmarkResult(double execTime, double avgCpuUsage, double avgUsedMemory, double avgFreeMemory) {
		super();
		this.execTime = execTime;
		this.avgCpuUsage = avgCpuUsage;
		this.avgUsedMemory = avgUsedMemory;
		this.avgFreeMemory = avgFreeMemory;
	}

	public BenchmarkResult() {
		super();
		execTime = 0.0;
		avgCpuUsage = 0.0;
		avgUsedMemory = 0.0;
		avgFreeMemory = 0.0;
	}

	public Regions getRegion() {
		return region;
	}

	public void setRegion(Regions region) {
		this.region = region;
	}

	public String getInstanceType() {
		return instanceType;
	}

	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}

	public int getNumProc() {
		return numProc;
	}

	public void setNumProc(int numProc) {
		this.numProc = numProc;
	}

	public int getProblemSize() {
		return problemSize;
	}

	public void setProblemSize(int problemSize) {
		this.problemSize = problemSize;
	}

	public double getExecTime() {
		return execTime;
	}

	public void setExecTime(double execTime) {
		this.execTime = execTime;
	}

	public double getAvgCpuUsage() {
		return avgCpuUsage;
	}

	public void setAvgCpuUsage(double avgCpuUsage) {
		this.avgCpuUsage = avgCpuUsage;
	}

	public double getAvgUsedMemory() {
		return avgUsedMemory;
	}

	public void setAvgUsedMemory(double avgUsedMemory) {
		this.avgUsedMemory = avgUsedMemory;
	}

	public double getAvgFreeMemory() {
		return avgFreeMemory;
	}

	public void setAvgFreeMemory(double avgFreeMemory) {
		this.avgFreeMemory = avgFreeMemory;
	}
	
	@Override
	public String toString() {
		return "BenchmarkResult [region=" + region + ", instanceType=" + instanceType + ", numProc=" + numProc
				+ ", problemSize=" + problemSize + ", execTime=" + execTime + ", avgCpuUsage=" + avgCpuUsage
				+ ", avgUsedMemory=" + avgUsedMemory + ", avgFreeMemory=" + avgFreeMemory + "]";
	}
}
