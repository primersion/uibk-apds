package at.ac.uibk.apds;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.simple.parser.ParseException;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.DescribeImagesRequest;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.Image;

public class AMIFiltering {
	
	public static Map<Regions, String> getAMIS(Map<Regions, AmazonEC2> ec2, List<Filter> filters) {
		Map<Regions, String> imageIds = new HashMap<Regions, String>();

		for(Entry<Regions, AmazonEC2> s : ec2.entrySet()){
			DescribeImagesRequest imageRequest = new DescribeImagesRequest().withFilters(filters);
			List<Image> images = s.getValue().describeImages(imageRequest).getImages();
			images.sort((i1, i2) -> i2.getCreationDate().compareTo(i1.getCreationDate()));
			imageIds.put(s.getKey(), images.get(0).getImageId());
		}
		return imageIds;
	}

	

	public static void printImage(Image image) {
		
		System.out.println("Architecture: " + image.getArchitecture());
		System.out.println("Creation Date: " + image.getCreationDate());
		System.out.println("Description: " + image.getDescription());
		System.out.println("ImageId: " + image.getImageId());
		System.out.println("OwnerAlias: " + image.getImageOwnerAlias());
		System.out.println("Name: " + image.getName());
		System.out.println("Virt.Type: " + image.getVirtualizationType());
		System.out.println("RootDeviceType: " + image.getRootDeviceType());
		System.out.println();
	}
	
	public static Map<Regions, String> getAMISForAllRegions(Map<Regions, AmazonEC2> ec2){

		/***************** Set a filter on available AMIs/VMIs ****************/
		List<Filter> filters = new LinkedList<Filter>();
		filters.add(new Filter().withName("image-type").withValues("machine"));
		filters.add(new Filter().withName("owner-alias").withValues("amazon"));
		filters.add(new Filter().withName("name").withValues("amzn-ami-hvm*"));
		filters.add(new Filter().withName("virtualization-type").withValues("hvm"));
		filters.add(new Filter().withName("root-device-type").withValues("ebs"));

		Map<Regions, String> amiIds = getAMIS(ec2, filters);
		
		return amiIds;
	}

	/**
	 * @param args
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws IOException, ParseException, InterruptedException {

		Map<Regions, String> amiIds = getAMISForAllRegions(EC2Utils.createEC2forRegions(RegionUtils.getSelectedRegions()));

		for (Map.Entry<Regions, String> entry : amiIds.entrySet()) {
			System.out.println(entry.getKey() + " : " + entry.getValue());
		}
	}

}
