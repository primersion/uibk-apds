package at.ac.uibk.apds;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class CLIUtils {

	public static double[] vectorInputMenu(Scanner inputRdr) {

		double execTime, avgCpu, avgMem, price, latency;
		execTime = 0.0;
		avgCpu = 0.0;
		avgMem = 0.0;
		price = 0.0;
		latency = 0.0;
		
		boolean timeFlag, cpuFlag, memFlag, priceFlag = false, latencyFlag = false;

		try {
			while (true) {
				
				timeFlag = false;
				cpuFlag = false;
				memFlag = false;
				priceFlag = false;

				System.out.println(
						"Enter vector with the format <Execution time> <Avg CPU usage> <Avg Memory usage> [Price] [Latency](q to quit): ");
				
				
				
				if (inputRdr.hasNextDouble()) {
					execTime = inputRdr.nextDouble();
					timeFlag = true;
				}
				else {	
					if (inputRdr.hasNext()) {
						if(inputRdr.next().toLowerCase().equals("q")) {
							System.exit(0);
						}
						else {
							System.out.println("Execution time has incorrect format.");
						}
					}
					
				}
				
				if (inputRdr.hasNextDouble()) {
					avgCpu = inputRdr.nextDouble();
					cpuFlag = true;
				}
				else {
					if (inputRdr.hasNext()) {
						System.out.println("Average CPU usage has incorrect format. ");
						inputRdr.next();
					}
					
				}
				
				if (inputRdr.hasNextDouble()) {
					avgMem = inputRdr.nextDouble();
					memFlag = true;
				}
				else {
					if (inputRdr.hasNext()) {
						System.out.println("Average memory usage has incorrect format. ");
						inputRdr.next();
					}
					
				}
				
				try {
					String[] input = inputRdr.nextLine().trim().split("\\s+");
					
					if(input.length > 0) {
						price = Double.valueOf(input[0]);
						priceFlag = true;
					}
					
					if(input.length > 1) {
						latency = Double.valueOf(input[1]);
						latencyFlag = true;
					}
					
				}
				catch (Exception e) {
					//price and latency optional ignore
				}
				
				if(!timeFlag || !cpuFlag || !memFlag) {
					continue;
				}
				
				if((execTime <= 0.0 || execTime > 10.0) 
						|| (avgCpu <= 0.0 || avgCpu > 10.0)
						|| (avgMem <= 0.0 || avgMem > 10.0)
						|| (priceFlag && (price <= 0.0 || price > 10.0))
						|| (latencyFlag && (latency <= 0.0 || latency > 10.0))) {
					System.out.println("Values must be in range ]0.0 - 10.0]");
					continue;
				}
				else {
					break;
				}
			}
		}

		catch (Exception e) {
			System.out.println("There was an error reading the input: " + e.getMessage());
		}

		double[] vec;
		
		if(priceFlag && latencyFlag) {
			vec = new double[] { execTime, avgCpu, avgMem, price, latency };
			System.out.println("Created vector: [" + execTime + ", " + avgCpu + ", " + avgMem + ", " + price + ", " + latency + "]");
		}
		else if(priceFlag) {
			vec = new double[] { execTime, avgCpu, avgMem, price };
			System.out.println("Created vector: [" + execTime + ", " + avgCpu + ", " + avgMem + ", " + price + "]");
		}
		else {
			vec = new double[] { execTime, avgCpu, avgMem};
			System.out.println("Created vector: [" + execTime + ", " + avgCpu + ", " + avgMem + "]");
		}

		return vec;

	}

	public static InstanceVector instanceChoiceMenu(Scanner inputRdr, ArrayList<? extends InstanceVector> instanceList) {
		int instanceId = 0;
		String sInstance;
		for (InstanceVector instance : instanceList) {
			sInstance = String.format("%1$03d: %2$s", instanceId, instance);
			System.out.println(sInstance);
			instanceId++;
		}

		System.out.println();

		int choice = 0;
		
		if (inputRdr.hasNextLine()) {
			inputRdr.nextLine();
		}

		try {
			while (true) {
				System.out.println("Enter instance number to start (q to quit): ");

				if (inputRdr.hasNextInt()) {
					choice = inputRdr.nextInt();
					inputRdr.nextLine();
					if (choice >= instanceList.size()) {
						System.out.println("Invalid instance number entered.");
						continue;
					}

					break;
				} else {
					if (inputRdr.hasNextLine() && inputRdr.nextLine().toLowerCase().equals("q")) {
						System.exit(0);
					} else {
						System.out.println("Choice has incorrect format. ");
						continue;
					}
				}
			}
		} catch (Exception e) {
			System.out.println("There was an error reading the input: " + e.getMessage());
		}

		InstanceVector instance = instanceList.get(choice);
		return instance;

	}

	public static boolean useExistingLogMenu(Scanner inputRdr) {
		File logFolder = new File(S3Utils.S3_LOCAL_DOWNLOAD_FOLDER);
		String input = "";

		if (directoryNotEmpty(logFolder)) {
			try {
				while (true) {
					System.out.println("Old benchmark results exist. Use local files? (y/n) (q to quit): ");

					if (inputRdr.hasNextLine()) {
						input = inputRdr.nextLine().toLowerCase();
					}

					if (input.equals("y") || input.equals("yes")) {
						return true;
					} else if (input.equals("n") || input.equals("no")) {
						return false;
					} else if (input.equals("q") || input.equals("quit")) {
						System.exit(0);
					} else {
						System.out.println("Input has incorrect format. ");
						continue;
					}
				}
			} catch (Exception e) {
				System.out.println("There was an error reading the input: " + e.getMessage());
				return false;
			}

		} else {
			return false;
		}
	}

	private static boolean directoryNotEmpty(File dir) {
		if (dir.isDirectory()) {
			File[] files = dir.listFiles();
			if (files != null && files.length > 0) {
				return true;
			}

			return false;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {
		Scanner inputRdr = new Scanner(System.in);

		vectorInputMenu(inputRdr);
	}

}
