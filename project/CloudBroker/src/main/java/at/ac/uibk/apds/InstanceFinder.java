package at.ac.uibk.apds;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.TreeMap;

public class InstanceFinder {

	private ArrayList<BenchmarkResult> benchmarkResults;
	private ArrayList<InstanceVector> basis = new ArrayList<InstanceVector>();
	private AWSPriceListJSON priceList;

	private ArrayList<InstanceVector> decMat;
	private InstanceVector ideal;
	private InstanceVector antiIdeal;
	private InstanceVector query;

	private ArrayList<InstanceVectorL2> decMatTopsisL2;
	private InstanceVectorL2 idealTopsisL2;
	private InstanceVectorL2 antiIdealTopsisL2;
	private InstanceVectorL2 queryTopsisL2;

	private ArrayList<InstanceVectorCos> decMatTopsisCos;
	private InstanceVectorCos idealTopsisCos;
	private InstanceVectorCos antiIdealTopsisCos;
	private InstanceVectorCos queryTopsisCos;

	private TreeMap<String, String> pingRegions;
	private String[][] pingList;
	
	public InstanceFinder(){
		this(BenchmarkUtils.readLogFolder(S3Utils.S3_LOCAL_DOWNLOAD_FOLDER), AWSPriceListJSON.getInstance(), DataMapper.getRegionPings());
	}

	public InstanceFinder(ArrayList<BenchmarkResult> benchmarkResults, AWSPriceListJSON priceList, String[][] pingList) {
		this.benchmarkResults = benchmarkResults;
		this.priceList = priceList;
		pingRegions = RegionUtils.getPingRegions();
		this.pingList = pingList;

		for (BenchmarkResult br : benchmarkResults) {
			// System.out.println(br);
			String regionCode = br.getRegion().getName();
			String instanceType = br.getInstanceType();
			double price = priceList.getPriceRegionInstanceType(regionCode, instanceType);
			this.basis.add(new InstanceVector(regionCode, instanceType, new double[] { br.getExecTime(),
					br.getAvgCpuUsage(), br.getAvgUsedMemory(), price, getPingRegion(regionCode) }));
		}
	}

	private double getPingRegion(String regionCode) {
		String region = pingRegions.get(regionCode);
		double ping = Double.MAX_VALUE;
		for (String[] entry : pingList) {
			if (entry[1].compareTo(region) == 0) {
				ping = Double.valueOf(entry[0]);
				break;
			}
		}
		return ping;
	}

	public void rateInstances(double[] values) {

		values[0] = DataMapper.mapUserValueToData(getBenchmarkResults(), new Double(values[0]).intValue(),
				DataMapper.MappingType.EXECTIME);
		values[1] = DataMapper.mapUserValueToData(getBenchmarkResults(), new Double(values[1]).intValue(),
				DataMapper.MappingType.AVGCPUUSAGE);
		values[2] = DataMapper.mapUserValueToData(getBenchmarkResults(), new Double(values[2]).intValue(),
				DataMapper.MappingType.AVGMEMORYUSAGE);
		if (values.length >= 4) {
			values[3] = DataMapper
					.mapUserValueToPrice(getPriceList().getMachines(), "", new Double(values[3]).intValue())
					.getPricePerHour();
		}
		if (values.length == 5) {
			values[4] = Double.valueOf(DataMapper.mapUserDataToRegionLatency(new Double(values[4]).intValue(), pingList)[0]);
		}

		this.query = new InstanceVector(values);
		System.out.println();
		System.out.println(" ID: " + InstanceVector.fixedLengthString("Region", 15) + InstanceVector.fixedLengthString("Instance", 15) + InstanceVector.fixedLengthString("Similarity", 15) + InstanceVector.fixedLengthString("Original vector", 50) + InstanceVector.fixedLengthString("Normalized vector", 50));
		System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.println("ASK: " + query);
		System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------");
		this.decMat = new ArrayList<InstanceVector>();
		for (InstanceVector ins : this.basis) {
			this.decMat.add(new InstanceVector(ins.getRegion(), ins.getInstanceType(),
					Arrays.copyOfRange(ins.getValuesOrig(), 0, values.length)));
		}
		calculateBounds(this.basis.get(0).getValuesOrig().length);
		for (InstanceVector ins : this.decMat) {
			ins.calculateSimilarity(this.query);
		}
		Collections.sort(this.decMat);
	}

	private void calculateBounds(int length) {
		double max[] = new double[length];
		double min[] = new double[length];
		for (int i = 0; i < length; i++) {
			min[i] = 99999;
		}

		for (InstanceVector instance : this.basis) {
			double[] values = instance.getValues();
			for (int i = 0; i < length; i++) {
				if (min[i] > values[i]) min[i] = values[i];
				if (max[i] < values[i]) max[i] = values[i];
			}
		}
		this.ideal = new InstanceVector("ideal", "", min);
		this.antiIdeal = new InstanceVector("antiIdeal", "", max);
	}

	public void rateInstancesTopsisL2(double[] weights) {
		this.decMatTopsisL2 = new ArrayList<InstanceVectorL2>();
		this.queryTopsisL2 = new InstanceVectorL2("", "", weights);
		calculateDecisionMatrixL2(weights);
		calculateBoundsTopsisL2(weights.length);
		orderInstancesTopsisL2();
	}

	private void calculateDecisionMatrixL2(double[] weights) {
		double sum = 0.0;
		for (int i = 0; i < weights.length; i++) {
			sum += weights[i];
		}
		double[] w = new double[weights.length];
		for (int i = 0; i < weights.length; i++) {
			w[i] = weights[i] / sum;
		}
		for (InstanceVector ins : this.basis) {
			this.decMatTopsisL2.add(new InstanceVectorL2(ins.getRegion(), ins.getInstanceType(),
					Arrays.copyOfRange(ins.getValuesOrig(), 0, w.length), w));
		}
	}

	private void calculateBoundsTopsisL2(int length) {
		double max[] = new double[length];
		double min[] = new double[length];
		for (int i = 0; i < length; i++) {
			min[i] = 99999;
		}

		for (InstanceVector instance : this.decMatTopsisL2) {
			double[] values = instance.getValues();
			for (int i = 0; i < length; i++) {
				if (min[i] > values[i]) min[i] = values[i];
				if (max[i] < values[i]) max[i] = values[i];
			}
		}
		this.idealTopsisL2 = new InstanceVectorL2("ideal", "", min);
		this.antiIdealTopsisL2 = new InstanceVectorL2("antiIdeal", "", max);
	}

	private void orderInstancesTopsisL2() {
		for (InstanceVectorL2 ins : this.decMatTopsisL2) {
			ins.calculateSimilarity(this.idealTopsisL2, this.antiIdealTopsisL2);
		}
		Collections.sort(this.decMatTopsisL2);
	}

	public void rateInstancesTopsisCos(double[] weights) {
		this.decMatTopsisCos = new ArrayList<InstanceVectorCos>();
		this.queryTopsisCos = new InstanceVectorCos("", "", weights);
		calculateDecisionMatrixCos(weights);
		calculateBoundsTopsisCos(weights.length);
		orderInstancesTopsisCos();
	}

	private void calculateDecisionMatrixCos(double[] weights) {
		double sum = 0.0;
		for (int i = 0; i < weights.length; i++) {
			sum += weights[i];
		}
		double[] w = new double[weights.length];
		for (int i = 0; i < weights.length; i++) {
			w[i] = weights[i] / sum;
		}

		for (InstanceVector ins : this.basis) {
			this.decMatTopsisCos.add(new InstanceVectorCos(ins.getRegion(), ins.getInstanceType(),
					Arrays.copyOfRange(ins.getValuesOrig(), 0, w.length), w));
		}
	}

	private void calculateBoundsTopsisCos(int length) {
		double max[] = new double[length];
		double min[] = new double[length];
		for (int i = 0; i < length; i++) {
			min[i] = 99999;
		}

		for (InstanceVector instance : this.decMatTopsisCos) {
			double[] values = instance.getValues();
			for (int i = 0; i < length; i++) {
				if (min[i] > values[i]) min[i] = values[i];
				if (max[i] < values[i]) max[i] = values[i];
			}
		}
		this.idealTopsisCos = new InstanceVectorCos("ideal", "", min);
		this.antiIdealTopsisCos = new InstanceVectorCos("antiIdeal", "", max);
	}

	private void orderInstancesTopsisCos() {
		for (InstanceVectorCos ins : this.decMatTopsisCos) {
			ins.calculateSimilarity(this.idealTopsisCos, this.antiIdealTopsisCos);
		}
		Collections.sort(this.decMatTopsisCos);
	}

	public void printRankingCos() {
		System.out.println("Normal Cosine Similarity");
		System.out.println("Query: " + this.query);

		for (InstanceVector v : this.decMat) {
			System.out.println(v);
		}
		System.out.println();
	}

	public void printRankingTopsisCos() {
		System.out.println("TOPSIS Cosine Similarity");
		System.out.println("Query: " + this.queryTopsisCos);
		for (InstanceVector v : this.decMatTopsisCos) {
			System.out.println(v);
		}
		System.out.println("\n");
	}

	public void printRankingTopsisL2() {
		System.out.println("TOPSIS Euclidean Distance");
		System.out.println("Query: " + this.queryTopsisL2);
		for (InstanceVector v : this.decMatTopsisL2) {
			System.out.println(v);
		}
		System.out.println("\n");
	}

	public InstanceVector getIdeal() {
		return ideal;
	}

	public InstanceVector getAntiIdeal() {
		return antiIdeal;
	}

	public ArrayList<? extends InstanceVector> getDecMat() {
		if(decMat != null)
			return decMat;
		if(decMatTopsisCos != null)
			return decMatTopsisCos;
		return decMatTopsisL2;
	}

	public ArrayList<InstanceVectorL2> getDecMatTopsisL2() {
		return decMatTopsisL2;
	}
	
	public InstanceVector getIdealVector(){
		if(ideal != null)
			return ideal;
		if(idealTopsisCos != null)
			return idealTopsisCos;
		return idealTopsisL2;
	}
	
	public InstanceVector getAntiIdealVector(){
		if(antiIdeal != null)
			return antiIdeal;
		if(antiIdealTopsisCos != null)
			return antiIdealTopsisCos;
		return antiIdealTopsisL2;
	}

	public ArrayList<InstanceVectorCos> getDecMatTopsisCos() {
		return decMatTopsisCos;
	}

	public ArrayList<BenchmarkResult> getBenchmarkResults() {
		return benchmarkResults;
	}

	public AWSPriceListJSON getPriceList() {
		return this.priceList;
	}
	
	public InstanceVector getQueryVector(){
		if(query != null)
			return query;
		if(queryTopsisCos != null)
			return queryTopsisCos;
		return queryTopsisL2;
	}

	public static void main(String[] args) {

		InstanceFinder iF = new InstanceFinder();

		iF.rateInstances(new double[] { 7.0, 1.0, 1.0});
		iF.printRankingCos();

		iF.rateInstances(new double[] { 7.0, 1.0, 1.0, 10.0});
		iF.printRankingCos();
		
		iF.rateInstances(new double[] { 7.0, 1.0, 1.0, 10.0, 3.0});
		iF.printRankingCos();

//		iF.rateInstancesTopsisL2(new double[] { 7.0, 1.0, 1.0 });
//		iF.printRankingTopsisL2();
//
//		iF.rateInstancesTopsisL2(new double[] { 7.0, 1.0, 1.0, 10.0 });
//		iF.printRankingTopsisL2();
//
//		iF.rateInstancesTopsisCos(new double[] { 7.0, 1.0, 1.0 });
//		iF.printRankingTopsisCos();
//
//		iF.rateInstancesTopsisCos(new double[] { 7.0, 1.0, 1.0, 10.0 });
//		iF.printRankingTopsisCos();

	}

}
