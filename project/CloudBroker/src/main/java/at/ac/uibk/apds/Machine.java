package at.ac.uibk.apds;

public class Machine {

	private String region;
	private String instanceType;
	private double pricePerHour;

	public Machine(String r, String it, double p) {

		this.region = r;
		this.instanceType = it;
		this.pricePerHour = p;
	}

	public String getRegion() {
		return this.region;
	}

	public String getInstanceType() {
		return this.instanceType;
	}

	public double getPricePerHour() {
		return this.pricePerHour;
	}

	@Override
	public String toString() {
		return "Machine [region=" + region + ", instanceType=" + instanceType + ", pricePerHour=" + pricePerHour + "]";
	}

	public String toCSVString(){
		return region + "," + instanceType + "," + pricePerHour;
	}
}