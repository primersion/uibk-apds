package at.ac.uibk.apds;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

public class BenchmarkEval {
	
	private static final double MAX_BUDGET = 100;

	public static void main(String[] args){
		boolean isTest = false;
		
		AmazonS3 s3 = AmazonS3ClientBuilder.standard().withRegion(Regions.EU_CENTRAL_1).build();

		S3Utils.createBucket(s3, S3Utils.S3_LOG_BUCKET_NAME);
		
		AWSPriceListJSON priceList = AWSPriceListJSON.getInstance();
		double priceFull = priceList.getPriceFullBenchmark();

		int count = 0;
		final int max = 24;
		while(count < max && MAX_BUDGET - priceFull * (count + 1) > 0){
			System.out.println("Starting benchmark " + count + 1 + " of " + max);
			long starttime = System.currentTimeMillis();
			
			String timestamp = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
			
			InstanceManager im = new InstanceManager(isTest);
			
			System.out.println("Deleting previous log folders");
			S3Utils.deleteS3Folder(s3, S3Utils.S3_LOG_BUCKET_NAME, "log");
	
			// Start all specified instances
			im.startAll();
			
			// Wait for all benchmarks to finish
			S3Utils.waitBenchmarkCompletion(s3, S3Utils.S3_LOG_BUCKET_NAME, "log", isTest);
			
			// Terminate benchmark instances
			im.terminateAll();
	
			// Download log files of benchmark
			System.out.println("Downloading log folder");
			S3Utils.downloadS3Folder(s3, S3Utils.S3_LOG_BUCKET_NAME, "log", System.getProperty("user.home") + "/log_eval/" + timestamp + "/");
			count++;
			
			System.out.println("Waiting until next hour");
			long dif = System.currentTimeMillis() - starttime;
			if(dif < 3600000){
				try {
					Thread.sleep(3600000 - dif);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
