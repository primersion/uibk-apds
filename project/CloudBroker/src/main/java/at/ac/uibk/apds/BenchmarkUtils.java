package at.ac.uibk.apds;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import com.amazonaws.regions.Regions;

public class BenchmarkUtils {

	public static BenchmarkResult resultFromCSV(String csvFile) {
		BufferedReader br;

		try {
			br = new BufferedReader(new FileReader(csvFile));
			String line;

			// Skip csv header
			for (int i = 1; i <= 7; i++) {
				br.readLine();
			}

			int entryCount = 0;

			double firstEpoch = 0.0;
			double lastEpoch = 0.0;
			double avgCpuUsage = 0.0;
			double avgUsedMemory = 0.0;
			double avgFreeMemory = 0.0;

			while ((line = br.readLine()) != null && !line.isEmpty()) {
				String[] fields = line.split(",");

				if (entryCount == 0) {
					firstEpoch = Double.valueOf(fields[0]);
				}

				lastEpoch = Double.valueOf(fields[0]);
				avgCpuUsage += Double.valueOf(fields[1]);
				avgUsedMemory += (Double.valueOf(fields[7]) / 1000000.0); // Convert
																			// to
																			// Megabyte
				avgFreeMemory += (Double.valueOf(fields[10]) / 1000000.0);
				entryCount += 1;
			}

			br.close();

			double executionTime = lastEpoch - firstEpoch;
			avgCpuUsage /= entryCount;
			avgUsedMemory /= entryCount;
			avgFreeMemory /= entryCount;

			return new BenchmarkResult(executionTime, avgCpuUsage, avgUsedMemory, avgFreeMemory);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static ArrayList<BenchmarkResult> readLogFolder(String path) {

		String[] instanceTypes = InstanceUtils.getInstanceTypes();
		Regions[] regions = RegionUtils.getSelectedRegions();
		ArrayList<BenchmarkResult> resultList = new ArrayList<>();

		for (Regions region : regions) {
			for (String instance : instanceTypes) {
				Path csvPath = Paths.get(path + region.getName() + "/" + instance + "/");
				BenchmarkResult result = new BenchmarkResult();
				
				result.setRegion(region);
				result.setInstanceType(instance);
				
				if (Files.exists(csvPath)) {
					String[] files = csvPath.toFile().list();
					for (String aFile : files) {
						BenchmarkResult iteration = resultFromCSV(csvPath + "/" + aFile);
						result.setExecTime(result.getExecTime() + iteration.getExecTime());
						result.setAvgCpuUsage(result.getAvgCpuUsage() + iteration.getAvgCpuUsage());
						result.setAvgUsedMemory(result.getAvgUsedMemory() + iteration.getAvgUsedMemory());
						result.setAvgFreeMemory(result.getAvgFreeMemory() + iteration.getAvgFreeMemory());
					}
					
					if(files.length > 0) {
						String[] fileFields = files[0].split("_");
						result.setProblemSize(Integer.valueOf(fileFields[1]));
						result.setNumProc(Integer.valueOf(fileFields[2]));
						
						result.setExecTime(result.getExecTime() / files.length);
						result.setAvgCpuUsage(result.getAvgCpuUsage() / files.length);
						result.setAvgUsedMemory(result.getAvgUsedMemory() / files.length);
						result.setAvgFreeMemory(result.getAvgFreeMemory() / files.length);
						
						resultList.add(result);
					}
				}
			}
		}

		return resultList;
	}

	public static void main(String[] args) {
		ArrayList<BenchmarkResult> list = BenchmarkUtils.readLogFolder(S3Utils.S3_LOCAL_DOWNLOAD_FOLDER);
		for (BenchmarkResult result : list) {
			System.out.println(result);
		}
	}

}
