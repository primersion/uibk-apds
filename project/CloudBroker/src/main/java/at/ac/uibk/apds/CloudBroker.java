package at.ac.uibk.apds;

import java.io.File;
import java.util.Scanner;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

public class CloudBroker {
	
	private static final boolean TESTMODE = true;

	public static void main(String[] args) {
		Scanner inputReader = new Scanner(System.in);
		InstanceManager im = new InstanceManager(TESTMODE);

		if (!CLIUtils.useExistingLogMenu(inputReader)) {
			// Setup S3 bucket and delete old benchmark results
			// Change settings of bucket, download folder in S3Utils.java
			//run.sh must be updated if region changed
			AmazonS3 s3 = AmazonS3ClientBuilder.standard().withRegion(Regions.EU_CENTRAL_1).build();

			S3Utils.createBucket(s3, S3Utils.S3_LOG_BUCKET_NAME);

			System.out.println("Deleting previous log folders");
			S3Utils.deleteS3Folder(s3, S3Utils.S3_LOG_BUCKET_NAME, "log");
			removeDirectory(new File(S3Utils.S3_LOCAL_DOWNLOAD_FOLDER));

			// Start all specified instances
			im.startAll();
			
			// Wait for all benchmarks to finish
			S3Utils.waitBenchmarkCompletion(s3, S3Utils.S3_LOG_BUCKET_NAME, "log", TESTMODE);
			
			// Terminate benchmark instances
			im.terminateAll();

			// Download log files of benchmark
			System.out.println("Downloading log folder");
			S3Utils.downloadS3Folder(s3, S3Utils.S3_LOG_BUCKET_NAME, "log", S3Utils.S3_LOCAL_DOWNLOAD_FOLDER);
		}

		// Get requirement vector from user
		double[] vec = CLIUtils.vectorInputMenu(inputReader);

		// Rate all instances
		System.out.println("Calculating instance similarities...");
		InstanceFinder vs = new InstanceFinder();
		vs.rateInstances(vec);

		// Get user choice and start instance
		InstanceVector instanceVec = CLIUtils.instanceChoiceMenu(inputReader, vs.getDecMat());
		inputReader.close();
		im.startFromVector(instanceVec);
	}

	public static void removeDirectory(File dir) {
		if (dir.isDirectory()) {
			File[] files = dir.listFiles();
			if (files != null && files.length > 0) {
				for (File aFile : files) {
					removeDirectory(aFile);
				}
			}
			dir.delete();
		} else {
			dir.delete();
		}
	}
}
