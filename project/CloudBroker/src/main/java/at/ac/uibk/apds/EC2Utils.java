package at.ac.uibk.apds;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DescribeKeyPairsRequest;
import com.amazonaws.services.ec2.model.DescribeKeyPairsResult;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsRequest;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsResult;
import com.amazonaws.services.ec2.model.KeyPairInfo;
import com.amazonaws.services.ec2.model.SecurityGroup;

public class EC2Utils {

	public static Map<Regions, AmazonEC2> createEC2forRegions(Regions[] regions){
		Map<Regions, AmazonEC2> ec2 = new HashMap<>();
		AWSCredentialsProvider credProvider = getDefaultCredsprovider();
		for (Regions region : regions) 
			ec2.put(region, AmazonEC2ClientBuilder.standard().withCredentials(credProvider).withRegion(region).build());
		return ec2;
	}
	
	public static AWSCredentialsProvider getDefaultCredsprovider() {
		try {
			AWSCredentials credentials = new ProfileCredentialsProvider("default").getCredentials();
			AWSStaticCredentialsProvider cp = new AWSStaticCredentialsProvider(credentials);
			return cp;
		} catch (Exception e) {
			throw new AmazonClientException("Cannot load the credentials from the credential profiles file. "
					+ "Please make sure that your credentials file is at the correct "
					+ "location (/home/<user>/.aws/credentials), and is in valid format.", e);
		}
	}
	
	public static List<SecurityGroup> getSecurityGroups(AmazonEC2 ec2) {
        DescribeSecurityGroupsRequest request = new DescribeSecurityGroupsRequest();
        DescribeSecurityGroupsResult result = ec2.describeSecurityGroups(request);
        return result.getSecurityGroups();
    }
	
	public static List<KeyPairInfo> getKeyPairs(AmazonEC2 ec2){
		DescribeKeyPairsRequest request = new DescribeKeyPairsRequest();
		DescribeKeyPairsResult result = ec2.describeKeyPairs(request);
		return result.getKeyPairs();
	}
}
