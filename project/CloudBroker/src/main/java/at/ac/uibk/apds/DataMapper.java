package at.ac.uibk.apds;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class DataMapper {
	
	
	public enum MappingType {
		EXECTIME, AVGCPUUSAGE, AVGMEMORYUSAGE
	}
	/**
	 * 
	 * @param list the list with all the benchmarkresults
	 * @param value nr between 1 and 10 from the user. 0 = worst, 10 = best
	 * @param type enum of the mapping type exectime, avgcpuusage or avgmemusage
	 * @return	returns the selected benchmark result with the Exec Time nearest to the wished user value
	 */
	public static double mapUserValueToData(ArrayList<BenchmarkResult> list, int value, MappingType type){
		// get min and max exec time from all results
		double min = 9999999;
		double max = -99999;
		for(BenchmarkResult result : list) {
			double BenchMarkedValue = 0.0;
			switch(type){
			case EXECTIME:
				BenchMarkedValue = result.getExecTime();
				break;
			case AVGCPUUSAGE:
				BenchMarkedValue = result.getAvgCpuUsage();
				break;
			case AVGMEMORYUSAGE:
				BenchMarkedValue = result.getAvgUsedMemory();
				break;
			}
			if(BenchMarkedValue < min)
				min = BenchMarkedValue;
			if(BenchMarkedValue > max)
				max = BenchMarkedValue;
		}
		
		// calc steps in between
		double step = (max - min) /10;
		double idealUserValue = min + step * (10 - value);
		System.out.println("ideal User Value: " + idealUserValue);

		//select the result with the nearest value to the user selected value
		min = 99999999;
		BenchmarkResult bestFit = null;
		for(BenchmarkResult result : list) {
			double diff = 0.0;
			switch(type){
			case EXECTIME:
				diff = Math.abs(idealUserValue - result.getExecTime());
				break;
			case AVGCPUUSAGE:
				diff = Math.abs(idealUserValue - result.getAvgCpuUsage());
				break;
			case AVGMEMORYUSAGE:
				diff = Math.abs(idealUserValue - result.getAvgUsedMemory());
			}
			if(diff < min){
				min = diff;
				bestFit =result;
			}
		}
		double ret = 0;
		switch(type){
		case EXECTIME:
			ret = bestFit.getExecTime();
			break;
		case AVGCPUUSAGE:
			ret = bestFit.getAvgCpuUsage();
			break;
		case AVGMEMORYUSAGE:
			ret = bestFit.getAvgUsedMemory(); 
		}
		return ret;
	}
	/**
	 * 
	 * @param list the list with all the benchmarkresults
	 * @param value nr between 1 and 10 from the user. 0 = worst, 10 = best
	 * @return	returns the selected benchmark result with the exec time nearest to the wished user value
	 */
	public static BenchmarkResult getExecTimeByMapping(ArrayList<BenchmarkResult> list, int value){
		// get min and max cpu time from all results
		double min = 9999999;
		double max = -99999;
		for(BenchmarkResult result : list) {
			double execTime = result.getExecTime();
			if(execTime < min)
				min = execTime;
			if(execTime > max)
				max = execTime;
		}
		
		// calc steps in between
		double step = (max - min) /10;
		double idealUserValue = min + step * (10 - value);
		System.out.println("ideal User Value: " + idealUserValue);
		//select the result with the nearest value to the user selected value
		min = 99999999;
		BenchmarkResult bestFit = null;
		for(BenchmarkResult result : list) {
			double diff = Math.abs(idealUserValue - result.getExecTime());
			if(diff < min){
				min = diff;
				bestFit =result;
			}
		}
		return bestFit;
	}
	
	/**
	 * 
	 * @param list the list with all the benchmarkresults
	 * @param value nr between 1 and 10 from the user. 0 = worst, 10 = best
	 * @return	returns the selected benchmark result with the Avg Cpu Usage nearest to the wished user value
	 */
	public static BenchmarkResult getAvgCpuUsageByMapping(ArrayList<BenchmarkResult> list, int value){
		// get min and max cpu time from all results
		double min = 9999999;
		double max = -99999;
		for(BenchmarkResult result : list) {
			double avgcpu = result.getAvgCpuUsage();
			if(avgcpu < min)
				min = avgcpu;
			if(avgcpu > max)
				max = avgcpu;
		}
		
		// calc steps in between
		double step = (max - min) /10;
		double idealUserValue = min + step * (10 - value);
		System.out.println("ideal User Value: " + idealUserValue);
		//select the result with the nearest value to the user selected value
		min = 99999999;
		BenchmarkResult bestFit = null;
		for(BenchmarkResult result : list) {
			double diff = Math.abs(idealUserValue - result.getAvgCpuUsage());
			if(diff < min){
				min = diff;
				bestFit =result;
			}
		}
		return bestFit;
	}
	
	
	/**
	 * 
	 * @param list the list with all the benchmarkresults
	 * @param value nr between 1 and 10 from the user. 0 = worst, 10 = best
	 * @return	returns the selected benchmark result with the Avg Used Memory nearest to the wished user value
	 */
	public static BenchmarkResult getAvgUsedMemoryByMapping(ArrayList<BenchmarkResult> list, int value){
		// get min and max cpu time from all results
		double min = 9999999;
		double max = -99999;
		for(BenchmarkResult result : list) {
			double avgmem = result.getAvgCpuUsage();
			if(avgmem < min)
				min = avgmem;
			if(avgmem > max)
				max = avgmem;
		}
		
		// calc steps in between
		double step = (max - min) /10;
		double idealUserValue = min + step * (10 - value);
		System.out.println("ideal User Value: " + idealUserValue);
		//select the result with the nearest value to the user selected value
		min = 99999999;
		BenchmarkResult bestFit = null;
		for(BenchmarkResult result : list) {
			double diff = Math.abs(idealUserValue - result.getAvgCpuUsage());
			if(diff < min){
				min = diff;
				bestFit =result;
			}
		}
		return bestFit;
	}
	
	
	public static ArrayList<BenchmarkResult> getDatas(String Foldername){
		ArrayList<BenchmarkResult> resultList = new ArrayList<>();
		
		Path csvPath = Paths.get(Foldername);
		if(Files.exists(csvPath)) {
			String[] files = csvPath.toFile().list();
			    for (String aFile : files) {
			        BenchmarkResult result = BenchmarkUtils.resultFromCSV(csvPath + "/" + aFile);			        
			        resultList.add(result);
		    }
		}else{
			System.out.println("no files found in folder.");
		}

		
		return resultList;
	}
	
	public static double getAvgPing(String address){
		String ip = address;//"8.8.8.8";
		String opt = " -c 1";
		String pingResult = "";

		String pingCmd = "ping " + ip + opt;
		try {
		    Runtime r = Runtime.getRuntime();
		    Process p = r.exec(pingCmd);

		    BufferedReader in = new BufferedReader(new
		    InputStreamReader(p.getInputStream()));
		    String inputLine;
		    while ((inputLine = in.readLine()) != null) {
		        //System.out.println(inputLine);
		        pingResult = inputLine;
		    }
		    in.close();
		    
		} catch (IOException e) {
		    System.out.println(e);
		}
		
		String[] splitted = pingResult.split("/");
		double avgping = 0;
		if(splitted.length >= 4)
			avgping = Double.valueOf(splitted[4]);
		else
			avgping = -1;
		
		return avgping;
	}
	
	public static String[][] getRegionPings(){
		String[][] regions = {
				{"dynamodb.us-east-1.amazonaws.com", "US EAST - north virginia"},
				{"dynamodb.us-west-1.amazonaws.com", "US WEST -  north california"},
				{"dynamodb.ca-central-1.amazonaws.com","CA CENTRAL - canada"},
				{"dynamodb.eu-west-1.amazonaws.com","EU WEST - ireland"},
				{"dynamodb.eu-central-1.amazonaws.com", "EU CENTRAL - frankfurt"},
				{"dynamodb.ap-northeast-2.amazonaws.com", "AP NORTHEAST - seoul"},
				{"dynamodb.ap-southeast-2.amazonaws.com", "AP SOUTHEAST - sydney"},
				{"dynamodb.ap-northeast-1.amazonaws.com", "AP NORTHEAST - tokyo"},
				{"dynamodb.sa-east-1.amazonaws.com", "SA EAST - sao paolo"}
		};
		//ArrayList<Double> pingList = new ArrayList<Double>();
		System.out.println("getting pings from AWS regions ...");
		for (String[] region :regions){
			double ping = getAvgPing(region[0]);
			if(ping < 0){
				//pingList.add(-1); // -1 when timeout, ignoring this value
				region[0] = "-1";
				System.out.println(region[1] + ": timeout");
			}else{
				//pingList.add(ping);
				region[0] = String.valueOf(ping);
				System.out.println(region[1] + ": " + ping + "ms");
			}
		}
		return regions;
	}
	
	public static String[] mapUserDataToRegionLatency(int userValue, String[][] pingList){
		if(pingList == null)
			pingList = getRegionPings();
		// get min and max cpu time from all results
		double min = 9999999;
		double max = -99999;
		for(String[] regionPing : pingList) {
			double ping = Double.valueOf(regionPing[0]);
			if(ping < 0)
				continue;
			if(ping < min)
				min = ping;
			if(ping > max)
				max = ping;
		}
		
		// calc steps in between
		double step = (max - min) /10;
		double idealUserValue = min + step * (10 - userValue);
		System.out.println("ideal User Value: " + idealUserValue);
		
		//select the result with the nearest value to the user selected value
		min = 99999999;
		String[] bestFit = null;
		for(String[] regionPing : pingList) {
			double diff = Math.abs(idealUserValue - Double.valueOf(regionPing[0]));
			if(diff < min){
				min = diff;
				bestFit = regionPing;
			}
		}
		return bestFit;
	}
	
	/***
	 * this function expects a array list of machine which you get from the AWSPriceListJson class
	 * @param list of all machine with instance and hour prices
	 * @param region region you want prices for, if empty we look at all regions
	 * @param UserValue value from the user ( 0- 10 ) 
	 * @return best fit machine from list
	 */
	public static Machine mapUserValueToPrice(ArrayList<Machine> list, String region, int UserValue){
		double min = 9999.9;
		double max = -9999.9;
		for(Machine m : list){
			// if region not empty we skip if its not our region
			if(region != "")
				if(region != m.getRegion())
					continue;
			
			double price = m.getPricePerHour();
			if(min > price)
				min = price;
			if(max < price)
				max = price;
		}
		double step = (max-min) /10;
		double idealUserPrice = min + step * (10 - UserValue);
		System.out.println("ideal user price: " + idealUserPrice);
		min = 9999.9;
		Machine bestFit = null;
		for(Machine m :list){
			// if region not empty we skip if its not our region
			if(region != "")
				if(region != m.getRegion())
					continue;
			
			double diff = Math.abs(m.getPricePerHour() - idealUserPrice); 
			if(min > diff){
				min = diff;
				bestFit = m;
			}
		}
		return bestFit;
	} 
	
	public static void main(String[] args){
//		ArrayList<BenchmarkResult> list = new ArrayList<>();
//		for(int i = 1; i < 11; i ++){
//			list.add(new BenchmarkResult(i*3.6, 5.2*i, 2.4*i,3.6*i));
//		}
//		Collections.shuffle(list); //to make sure test datas are not sorted yet.
//		for(BenchmarkResult result : list) {
//			System.out.println(result);
//		}
		//System.out.println("selected Time: " + getExecTimeByMapping(list, 7).getExecTime());
		//System.out.println("selected avgCpuUsage: " + getAvgCpuUsageByMapping(list, 3).getAvgCpuUsage());
//		System.out.println("ret: " + mapUserValueToData(list, 5, MappingType.EXECTIME));
		String[] res =mapUserDataToRegionLatency(6, null);
		System.out.println("selected region for user");
		System.out.println(res[0] + " " + res[1]);
	}
}