package at.ac.uibk.apds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceStateChange;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;
import com.amazonaws.services.ec2.model.TerminateInstancesResult;

public class InstanceUtils {
	
	public static String[] getInstanceTypes(){
		return new String[]{
				"t2.micro",
				"t2.small",
				"t2.medium",
				"t2.large",
				"m3.medium",
				"m3.large",
//				"m4.large",
				"c4.large",
				"c4.xlarge",
				"r4.large",
				"r4.xlarge"
		};
	}
	
	public static Map<String, Void> getInstanceTypesAsMap(){
		Map<String, Void> map = new HashMap<>();
		for(String s : getInstanceTypes())
			map.put(s, null);
		return map;
	}
	
	public static String[] getTestInstanceType(){
		return new String[]{"t2.micro"};
	}
	
	public static List<Instance> waitUntilInstancesRunning(AmazonEC2 ec2, long interval, List<Instance> instances) {
		List<String> instanceIds = new ArrayList<>();
		for(Instance i : instances)
			instanceIds.add(i.getInstanceId());
		while (!instancesRunning(instances)) {
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			instances = requestUpdatedInstance(ec2, instanceIds, null);
		}
		return instances;
	}
	
	public static boolean instancesRunning(List<Instance> instances) {
		for (Instance i : instances) {
			if (!i.getState().getName().equals("running")) {
				System.out.println(
						"Waiting for instance " + i.getInstanceId() + " to start up. State: " + i.getState().getName());
				return false;
			}
		}
		return true;
	}
	
	public static List<Instance> requestUpdatedInstance(AmazonEC2 ec2, List<String> instanceId, List<Filter> filters) {
		DescribeInstancesRequest request = new DescribeInstancesRequest();
		request.setInstanceIds(instanceId);

		if (filters != null)
			request.setFilters(filters);

		List<Reservation> list = ec2.describeInstances(request).getReservations();
		List<Instance> instances = new LinkedList<Instance>();
		for (Reservation res : list)
			for (Instance ins : res.getInstances())
				instances.add(ins);
		return instances;
	}
	
	public static void terminateInstances(AmazonEC2 ec2, List<Instance> instances) {
		System.out.println("Shutting down instances");
		List<String> instanceIds = new ArrayList<>();
		for(Instance i : instances)
			instanceIds.add(i.getInstanceId());
		TerminateInstancesRequest terminateRequest = new TerminateInstancesRequest(instanceIds);
		TerminateInstancesResult terminateResult = ec2.terminateInstances(terminateRequest);
		for (InstanceStateChange s : terminateResult.getTerminatingInstances())
			System.out.println(
					"Stutdown initiated for instance id: " + s.getInstanceId() + " State: " + s.getCurrentState());
	}
}
