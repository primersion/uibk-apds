package at.ac.uibk.apds;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import com.amazonaws.regions.Regions;

public class RegionUtils {

	public static Regions[] getSelectedRegions(){
		return new Regions[]{
				Regions.EU_CENTRAL_1,		// frankfurt
				Regions.EU_WEST_1,			// ireland
				Regions.US_EAST_1,			// north virginia
				Regions.US_WEST_1,			// north california
				Regions.CA_CENTRAL_1,		// canada
				Regions.SA_EAST_1,			// sao paolo
				Regions.AP_NORTHEAST_1,		// tokyo
				Regions.AP_NORTHEAST_2,		// seoul
//				Regions.AP_SOUTH_1,			// mumbai, no m3 instanceType
				Regions.AP_SOUTHEAST_2,		// sydney
		};
	}
	
	public static String[] getSelectedRegionCodes() {
		return new String[]{
				"eu-central-1",
				"eu-west-1",
				"us-east-1",
				"us-west-1",
				"ca-central-1",
				"sa-east-1",
				"ap-northeast-1",
				"ap-northeast-2",
//				"ap-south-1",
				"ap-southeast-2"
		};
	}
	
	public static String[] getSelectedRegionNames(){
		return new String[]{
				"EU (Frankfurt)",
				"EU (Ireland)",
				"US East (N. Virginia)",
				"US West (N. California)",
				"Canada (Central)",
				"South America (Sao Paulo)",
				"Asia Pacific (Tokyo)",
				"Asia Pacific (Seoul)",
//				"Asia Pacific (Mumbai)",
				"Asia Pacific (Sydney)"
		};
	}
	
	public static TreeMap<String, String> getPingRegions(){
		TreeMap<String, String> pingRegions = new TreeMap<>();
		pingRegions.put("eu-central-1", "EU CENTRAL - frankfurt");
		pingRegions.put("eu-west-1", "EU WEST - ireland");
		pingRegions.put("us-east-1", "US EAST - north virginia");
		pingRegions.put("us-west-1", "US WEST -  north california");
		pingRegions.put("ca-central-1", "CA CENTRAL - canada");
		pingRegions.put("sa-east-1", "SA EAST - sao paolo");
		pingRegions.put("ap-northeast-1", "AP NORTHEAST - tokyo");
		pingRegions.put("ap-northeast-2", "AP NORTHEAST - seoul");
		pingRegions.put("ap-southeast-2", "AP SOUTHEAST - sydney");
		return pingRegions;
	}
	
	public static Map<String, Void> getSelectedRegionNamesAsMap(){
		Map<String, Void> map = new HashMap<>();
		for(String s : RegionUtils.getSelectedRegionNames())
			map.put(s, null);
		return map;
	}
	
	public static Map<String, String> getSelectedRegionsWithCodeAsMap() {
		Map<String, String> map = new HashMap<>();
		String[] names = getSelectedRegionNames();
		String[] codes = getSelectedRegionCodes();
		for (int i = 0; i < codes.length; i++) {
			map.put(names[i], codes[i]);
		}
		return map;
	}
	
	public static void main(String[] args) {
		Map<String, Void> map = RegionUtils.getSelectedRegionNamesAsMap();
		
		for (Map.Entry<String, Void> e : map.entrySet()) {
			System.out.println(e);
		}
	}
}
