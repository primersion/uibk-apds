package at.ac.uibk.apds;

import java.text.DecimalFormat;
import java.util.Arrays;

public class InstanceVector implements Comparable<InstanceVector> {

	private String region;
	private String instanceType;
	private double[] valuesOrig;
	private double[] values;
	private double similarity;

	public InstanceVector(String r, String i, double[] v) {
		this(r, i, v, v);
	}

	public InstanceVector(double[] v) {
		this("", "", v);
	}

	protected InstanceVector(String r, String i, double[] vO, double[] v) {
		boolean allZero = true;
		for (int idx = 0; idx < v.length; idx++) {
			if (v[idx] != 0.0) allZero = false;
		}
		if (allZero) throw new IllegalArgumentException("not all arguments must be equal zero");

		this.region = r;
		this.instanceType = i;
		this.valuesOrig = Arrays.copyOf(vO, vO.length);
		this.values = Arrays.copyOf(v, v.length);
		this.normalize();
	}

	private void normalize() {
		double length = this.length();
		for (int i = 0; i < this.values.length; i++) {
			this.values[i] = this.values[i] / length;
		}
	}

	private double dot(InstanceVector other) {
		double sum = 0.0;
		for (int i = 0; i < this.values.length; i++) {
			sum += this.values[i] * other.getValues()[i];
		}
		return sum;
	}

	private double length() {
		double sum = 0.0;
		for (int i = 0; i < this.values.length; i++) {
			sum += Math.pow(this.values[i], 2);
		}
		return Math.sqrt(sum);
	}

	public void calculateSimilarity(InstanceVector ideal, InstanceVector antiIdeal) {

	}

	public double calculateSimilarity(InstanceVector other) {
		double d = this.dot(other);
		double l1 = this.length();
		double l2 = other.length();
		this.setSimilarity(d / (l1 * l2));
		return this.getSimilarity();
	}

	public double calculateL2Distance(InstanceVector other) {
		double dist = 0.0;
		for (int i = 0; i < this.values.length; i++) {
			dist += Math.pow(this.values[i] - other.getValues()[i], 2);
		}
		return Math.sqrt(dist);
	}

	public String getRegion() {
		return region;
	}

	public String getInstanceType() {
		return instanceType;
	}

	protected void setValuesOrig(double[] v) {
		this.valuesOrig = v;
	}

	public double[] getValuesOrig() {
		return valuesOrig;
	}

	protected void setValues(double[] v) {
		this.values = v;
	}

	public double[] getValues() {
		return values;
	}

	protected void setSimilarity(double value) {
		this.similarity = value;
	}

	public double getSimilarity() {
		return similarity;
	}

	@Override
	public String toString() {
		DecimalFormat df = new DecimalFormat("#.#####");
		return "" + fixedLengthString(region, 15) + fixedLengthString(instanceType, 15) + fixedLengthString(df.format(similarity), 15) + fixedLengthString(doubleArrayToString(valuesOrig, "#.#####"), 50) + fixedLengthString(doubleArrayToString(values, "#.#####"), 50);
	}
	
	public String toCSVString(){
		return region + "," + instanceType + "," + similarity + "," + doubleArrayToCSV(valuesOrig, 5) + "," + doubleArrayToCSV(values, 5);
	}
	
	public static String doubleArrayToCSV(double[] array, int fill){
		DecimalFormat df = new DecimalFormat("#.##########");
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < array.length; i++){
			builder.append(df.format(array[i]));
			if(i < array.length - 1)
				builder.append(",");
		}
		if(array.length < fill){
			for(int i = 0; i < fill - array.length; i++)
				builder.append(",");
		}
		return builder.toString();
	}
	
	public static String fixedLengthString(String string, int length){
		return String.format("%1$" + length + "s", string);
	}
	
	public static String doubleArrayToString(double[] array, String format){
		DecimalFormat df = new DecimalFormat(format);
		StringBuilder builder = new StringBuilder("[");
		for(int i = 0; i < array.length; i++){
			builder.append(df.format(array[i]));
			if(i < array.length - 1)
				builder.append(", ");
		}
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int compareTo(InstanceVector o) {
		if (getSimilarity() < o.getSimilarity()) {
			return 1;
		} else if (getSimilarity() > o.getSimilarity()) {
			return -1;
		}
		return 0;
	}
}
