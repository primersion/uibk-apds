# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import csv
import codecs
import sys
from os.path import isfile

if len(sys.argv) < 2:
	print("Usage: python %s <csv>" % sys.argv[0])
	sys.exit(0)

if not isfile(sys.argv[1]):
	print("Error opening file")
	sys.exit(0)

titles = []
pings = {}
with codecs.open(sys.argv[1], "r", encoding='utf-8', errors='ignore') as csvFile:
	csvReader = csv.reader(csvFile, delimiter=',')

	# Skip header row
	next(csvReader)

	count = 1
	for row in csvReader:
		titles.append(str(count) + " " + row[0])
		pings[row[0]] = list(map(float, row[1:]))
		count += 1

ind = np.arange(0, len(pings.items()), 1)  # the x locations for the groups
width = 1 / 15       # the width of the bars

fig, ax = plt.subplots()
rects = []
count = -len(pings.items()) / 2
for (title, pingValues) in pings.items():
	rects.append(ax.bar(ind + count * width, pingValues, width))
	count += 1

# add some text for labels, title and axes ticks
ax.set_ylabel('Ping in ms')
ax.set_title('Pings of different AWS regions')
ax.set_xticks(ind + width / 2)
ax.set_xticklabels(range(1, len(pings.items()) + 1, 1))

ax.legend(rects, titles)

#plt.savefig('ping_graph.png')
plt.show()
