Average ping from Instance to Instance in ms. Values are the average of 10 pings.

Amazon Regions Pingtable | EU(Ireland) | EU(Frankfurt) | US East (N. Virginia) | US West (N. California) | Canada (Central) | South America (Sao Paulo) | Asia Pacific (Mumbai) | Asia Pacific (Tokyo) | Asia Pacific (Sydney) | Asia Pacific (Seoul) |
---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
**EU(Ireland)** | 0 | 22.384 | 76.342 | 137.802 | 77.541 | 184.045 | 119.132 | 216.379 | 298.521 | 237.475 |
**EU(Frankfurt)** | 22.423 | 0 | 88.918 | 147.155 | 98.752 | 204.978 | 109.387 | 227.884 | 322.746 | 256.998 |
**US East (N. Virginia)** | 82.351 | 89.076 | 0 | 61.995 | 15.198 | 122.856 | 180.943 | 159.573 | 225.497 | 170.861 |
**US West (N. California)** | 136.959 | 147.215 | 62.183 | 0 | 80.503 | 187.664 | 230.958 | 118.101 | 159.236 | 146.076 |
**Canada (Central)** | 77.496 | 98.805 | 15.208 | 77.371 | 0 | 123.871 | 194.067 | 151.412 | 230.912 | 169.757 |
**South America (Sao Paulo)** | 184.112 | 228.03 | 122.802 | 184.628 | 122.615 | 0 | 300.652 | 269.649 | 338.411 | 288.168 |
**Asia Pacific (Mumbai)** | 119.149 | 228.03 | 181.047 | 230.998 | 194.245 | 300.634 | 0 | 119.406 | 221.015 | 148.471 |
**Asia Pacific (Tokyo)** | 216.259 | 228.03 | 141.624 | 118.264 | 151.463 | 269.925 | 119.349 | 0 | 104.08 | 31.842 |
**Asia Pacific (Sydney)** | 300.773 | 322.62 | 226.969 | 159.235 | 230.928 | 338.386 | 220.755 | 104.108 | 0 | 132.576 |
**Asia Pacific (Seoul)** | 237.41 | 228.03 | 170.903 | 146.135 | 169.533 | 288.163 | 148.421 | 31.735 | 132.612 | 0  |
