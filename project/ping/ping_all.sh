#!/usr/bin/bash

timestamp() {
  date +"%Y-%m-%d_%H-%M-%S"
}

servers=(
  dynamodb.us-east-1.amazonaws.com
  dynamodb.us-west-1.amazonaws.com
  dynamodb.ca-central-1.amazonaws.com
  dynamodb.eu-west-1.amazonaws.com
  dynamodb.eu-central-1.amazonaws.com
  dynamodb.ap-northeast-2.amazonaws.com
  dynamodb.ap-southeast-2.amazonaws.com
  dynamodb.ap-northeast-1.amazonaws.com
  dynamodb.sa-east-1.amazonaws.com
)

server_names=(
  us_east_1
  us_west_1
  ca_central_1
  eu_west_1
  eu_central_1
  ap_notheast_2
  ap_southeast_2
  ap_northeast_1
  sa_east_1
)

starttime=$(timestamp)

num_servers=${#servers[@]}
for i in `seq 0 $(expr $num_servers - 1)`
do
  if [ ! -d ${server_names[$i]} ]; then
    mkdir ${server_names[$i]}
  fi
done

echo "Press [CTRL+C] to stop..."
num_pings=0
while :
do
  echo -ne "Number of pings finished: $num_pings"\\r
  num_pings=$(expr $num_pings + 1)
  
  for i in `seq 0 $(expr $num_servers - 1)`
  do
    filename="${server_names[$i]}/pings_$starttime.txt"
    echo -n "$(timestamp) " >> $filename
    ping -D -c 10 ${servers[$i]} | tail -1 >> $filename
  done
done
