# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import csv
import codecs
import sys
from os.path import isfile
import os
import glob

if len(sys.argv) < 3:
	print("Usage: python %s <data> <ending> [all in one graph]" % sys.argv[0])
	sys.exit(0)
	
allInOne = False
if len(sys.argv) == 4:
  allInOne = sys.argv[3]

#if not isfile(sys.argv[1]):
#	print("Error opening file")
#	sys.exit(0)
	
files = [file for file in glob.glob(sys.argv[1] + '/**/*' + sys.argv[2], recursive=True)]

plt.figure(figsize=(15,12))
nextColor = 0
for f in files:
  timestamps = []
  times = []
  with codecs.open(f, "r", encoding='utf-8', errors='ignore') as csvFile:
    csvReader = csv.reader(csvFile, delimiter=' ')

    try:
      for row in csvReader:
        timestamps.append(row[0])
        if len(row) < 5:
          times.append([None, None, None, None])
        else:
          times.append(row[4].split(os.sep))
    except IndexError:
      continue


  avg = []
  for tuple in times:
    avg.append(tuple[1])
  avg = avg[:1030]
  ind = np.arange(0, len(avg))

  colors = ['C0','C1','C2','C3','C4','C5','C6','C7','C8','C9']
  if not allInOne:
    plt.plot(ind, avg, colors[nextColor])
    #plt.xlabel("Time")
    plt.ylabel("Ping in ms")
    
    tick_x = range(0, len(avg), int(len(avg) / 5))
    tick_labels = [timestamps[i] for i in tick_x]
    plt.xticks(tick_x, tick_labels)
    
    plt.savefig(f + '.png')
    plt.clf()
    print("Plot for %s saved successfully" % f)
  else:
    plt.plot(ind, avg, colors[nextColor], label=f.split(os.sep)[1])
  nextColor += 1

if allInOne:
  #plt.xlabel("Time")
  plt.ylabel("Ping in ms")
  
  tick_x = range(0, len(avg) + 1, int(len(avg) / 5))
  tick_labels = [timestamps[i] for i in tick_x]
  plt.xticks(tick_x, tick_labels)
  plt.axis([0, len(avg), 0, 500])
    
  plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)
  
  filename = sys.argv[1] + '/all_in_one.png'
  plt.savefig(filename)
  print("Plot for %s saved successfully" % filename)
