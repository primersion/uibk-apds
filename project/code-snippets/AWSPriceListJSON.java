
// needs JSON-library from here: http://www.java2s.com/Code/Jar/j/Downloadjavajsonjar.htm 

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.GenericArrayType;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class loads the current offer-file for Amazon EC2 and extracts the
 * price-per-hour for some specific instance-types in some specific regions
 */
public class AWSPriceListJSON {

	private String ec2OfferURL;
	private List<Machine> machines = new ArrayList<Machine>();
	private List<String> regions = new ArrayList<String>();
	private List<String> instanceTypes = new ArrayList<String>();

	public AWSPriceListJSON(String ec2Offer) {

		super();
		this.ec2OfferURL = ec2Offer;
		init_regions();
		init_instanceTypes();
	}

	/**
	 * Regions that should be checked
	 */
	private void init_regions() {

		this.regions.add("EU (Frankfurt)");
		this.regions.add("EU (Ireland)");
		this.regions.add("US East (N. Virginia)");
		this.regions.add("US West (N. California)");
		this.regions.add("US West (N. California)");
		this.regions.add("Canada (Central)");
		this.regions.add("South America (Sao Paulo)");
		this.regions.add("Asia Pacific (Mumbai)");
		this.regions.add("Asia Pacific (Tokyo)");
		this.regions.add("Asia Pacific (Sydney)");
		this.regions.add("Asia Pacific (Seoul)");
	}

	/**
	 * Instance-Types that should be checked
	 */
	private void init_instanceTypes() {

		this.instanceTypes.add("t2.small");
		this.instanceTypes.add("t2.medium");
		this.instanceTypes.add("t2.large");
		this.instanceTypes.add("m3.medium");
		this.instanceTypes.add("m3.large");
		this.instanceTypes.add("m4.large");
		this.instanceTypes.add("c4.large");
		this.instanceTypes.add("c4.xlarge");
		this.instanceTypes.add("r4.large");
		this.instanceTypes.add("r4.xlarge");
	}

	
	/**
	 * Processes the offer-file
	 */
	public void getOfferFile() {

		try {

			JSONObject offer_file = new JSONObject(readOfferFileFromUrl());
			JSONObject products = (JSONObject) offer_file.get("products");
			JSONObject terms = (JSONObject) ((JSONObject) offer_file.get("terms")).get("OnDemand");
			JSONArray product_names = products.names();

			String offerTermCode = "JRTCKXETXF";
			String priceDimCode = "6YS6EN2CT7";

			for (int i = 0; i < product_names.length(); i++) {

				String sku = product_names.get(i).toString();
				JSONObject attributes = (JSONObject) ((JSONObject) products.get(sku)).get("attributes");

				try {

					String location = attributes.getString("location");
					String instanceType = attributes.getString("instanceType");
					String operatingSystem = attributes.getString("operatingSystem");

					if (operatingSystem.equals("Linux") && this.regions.contains(location)
							&& this.instanceTypes.contains(instanceType)) {
						JSONObject product_terms = (JSONObject) terms.get(sku);
						JSONObject price_per_unit = (JSONObject) ((JSONObject) ((JSONObject) ((JSONObject) product_terms
								.get(sku + "." + offerTermCode)).get("priceDimensions"))
										.get(sku + "." + offerTermCode + "." + priceDimCode)).get("pricePerUnit");
						machines.add(new Machine(location, instanceType, price_per_unit.getDouble("USD")));
					}

				} catch (JSONException e) {
					continue;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		machines.sort((m1, m2) -> m1.getRegion().compareTo(m2.getRegion()));

		for (Machine m : machines) {
			System.out.println(m);
		}

	}

	/**
	 * Reads offer-file from URL
	 * @return returns the offer-file as String
	 */
	private String readOfferFileFromUrl() {

		BufferedReader reader = null;
		int chars_read = 0;

		try {

			URL url = new URL(this.ec2OfferURL);
			reader = new BufferedReader(new InputStreamReader(url.openStream()));
			StringBuffer buffer = new StringBuffer();
			char[] chars = new char[1024];
			while ((chars_read = reader.read(chars)) != -1) {
				buffer.append(chars, 0, chars_read);
			}
			return buffer.toString();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return "";
	}

	public static void main(String[] args) {

		AWSPriceListJSON prices = new AWSPriceListJSON(
				// "https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/AmazonEC2/current/eu-central-1/index.json");
				"https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/AmazonEC2/current/index.json");

		prices.getOfferFile();
	}

	private class Machine {
		String region;
		String instanceType;
		Double pricePerHour;

		public Machine(String r, String it, Double p) {

			this.region = r;
			this.instanceType = it;
			this.pricePerHour = p;
		}

		public String getRegion() {
			return this.region;
		}

		public String getInstanceType() {
			return this.instanceType;
		}

		public Double getPricePerHour() {
			return this.pricePerHour;
		}

		@Override
		public String toString() {
			return "Machine [region=" + region + ", instanceType=" + instanceType + ", pricePerHour=" + pricePerHour
					+ "]";
		}

	}

}
