import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.simple.parser.ParseException;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DescribeImagesRequest;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.Image;

public class AMIFiltering {

	static AmazonEC2 ec2;
	static AWSCredentialsProvider credProvider;

	static List<Regions> our_regions;

	static public Map<Regions, String> getAMIS(AWSCredentialsProvider credProvider, List<Filter> filters) {

		Map<Regions, String> imageIds = new HashMap<Regions, String>();

		for (Regions region : our_regions) {

			ec2 = AmazonEC2ClientBuilder.standard().withCredentials(credProvider).withRegion(region).build();
			// System.out.println("Region: " + region);

			DescribeImagesRequest imageRequest = new DescribeImagesRequest().withFilters(filters);
			List<Image> images = ec2.describeImages(imageRequest).getImages();
			images.sort((i1, i2) -> i2.getCreationDate().compareTo(i1.getCreationDate()));
			imageIds.put(region, images.get(0).getImageId());
		}
		return imageIds;
	}

	public static AWSCredentialsProvider getDefaultCredsprovider() {

		try {
			AWSCredentials credentials = new ProfileCredentialsProvider("default").getCredentials();
			AWSStaticCredentialsProvider cp = new AWSStaticCredentialsProvider(credentials);
			return cp;
		} catch (Exception e) {
			throw new AmazonClientException("Cannot load the credentials from the credential profiles file. "
					+ "Please make sure that your credentials file is at the correct "
					+ "location (/home/<user>/.aws/credentials), and is in valid format.", e);
		}
	}

	public static List<Regions> getSelectedRegions() {

		List<Regions> tmpList = new ArrayList<Regions>();
		tmpList.add(Regions.AP_SOUTHEAST_2); // sydney
		tmpList.add(Regions.AP_NORTHEAST_1); // tokyo
		tmpList.add(Regions.AP_SOUTH_1); // mumbai
		tmpList.add(Regions.SA_EAST_1); // sao paulo
		tmpList.add(Regions.AP_NORTHEAST_2); // seoul
		tmpList.add(Regions.EU_WEST_1); // irland
		tmpList.add(Regions.EU_CENTRAL_1); // frankfurt
		tmpList.add(Regions.CA_CENTRAL_1); // canada
		tmpList.add(Regions.US_WEST_1); // north virginia
		tmpList.add(Regions.US_EAST_1); // north carolina
		return tmpList;
	}

	public static void printImage(Image image) {

		System.out.println("Architecture: " + image.getArchitecture());
		System.out.println("Creation Date: " + image.getCreationDate());
		System.out.println("Description: " + image.getDescription());
		System.out.println("ImageId: " + image.getImageId());
		System.out.println("OwnerAlias: " + image.getImageOwnerAlias());
		System.out.println("Name: " + image.getName());
		System.out.println("Virt.Type: " + image.getVirtualizationType());
		System.out.println("RootDeviceType: " + image.getRootDeviceType());
		System.out.println();
	}

	/**
	 * @param args
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws IOException, ParseException, InterruptedException {

		our_regions = getSelectedRegions();
		credProvider = getDefaultCredsprovider();

		/***************** Set a filter on available AMIs/VMIs ****************/
		List<Filter> filters = new LinkedList<Filter>();
		filters.add(new Filter().withName("image-type").withValues("machine"));
		filters.add(new Filter().withName("owner-alias").withValues("amazon"));
		filters.add(new Filter().withName("name").withValues("amzn-ami-hvm*"));
		filters.add(new Filter().withName("virtualization-type").withValues("hvm"));
		filters.add(new Filter().withName("root-device-type").withValues("ebs"));

		Map<Regions, String> amiIds = getAMIS(credProvider, filters);

		for (Map.Entry<Regions, String> entry : amiIds.entrySet()) {
			System.out.println(entry.getKey() + " : " + entry.getValue());
		}

	}

}
