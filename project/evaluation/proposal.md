############# LATENCY #############
- Create free micro instance
- Checks latency to all regions periodically every hour for one week
- See fluctuations in latency depending on time of day / weekday
[ From where should the latency measure be executed? ]

############# RUNTIME ##############
- Running benchmark once costs approx. 12.86 USD/h
- Budget equals 400 USD -> approx. 31 runs
- Run on different weekdays -> 31 / 7 = 4 times a day
- Run on different times of day -> 24 / 4 = 6 -> every 6 hours (2am, 8am, 2pm, 8pm)
- Create free micro instance
- Runs benchmark periodically every 6 hours and collects results
[ Run only on 2 regions ]

############# CLOUD BROKER ##############
- Evaluate cloud broker by using the results of the evaluation above
- Vary values for latency, price, runtime from above
- Vary user input values on how important a point is
- See results for different time of day / weekday / different models







[ Changes only every two weeks -> not relevant for evaluation ]
[############# PRICE ############]
[- Create free micro instance]
[- Checks instance prices several times a day]
[- See fluctuations in price and run instance when cheapest]

