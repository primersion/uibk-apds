import numpy as np
import matplotlib.pyplot as plt
import csv
import codecs
import sys
from os.path import isfile
import os
from glob import glob
import matplotlib.cm as cm

import pandas as pd
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from matplotlib import*
from matplotlib.cm import register_cmap
from scipy import stats

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA

from mpl_toolkits.mplot3d import proj3d
from matplotlib.patches import FancyArrowPatch

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

from collections import OrderedDict

if len(sys.argv) < 2:
	print("Usage: python %s <data> [allInOne] [regionList]" % sys.argv[0])
	sys.exit(0)

allInOne = False	
if len(sys.argv) > 2:
  allInOne = True
  
regionList = None
if len(sys.argv) > 3:
  regionList = sys.argv[3].split()

files = glob(sys.argv[1] + "*")

if allInOne:
  fig = plt.figure(figsize=(15,12))
  ax = fig.add_subplot(111, projection='3d')
  
instanceSet = [
  "t2.micro","t2.small","t2.medium","t2.large","m3.medium",
  "m3.large","r4.large","r4.xlarge","c4.large","c4.xlarge"
]
for f in files:
  data = []
  instanceLabel = []
  instance = []
  
  topsis = False
  if "topsis" in f:
    topsis = True
  
  with codecs.open(f, "r", encoding='utf-8', errors='ignore') as csvFile:
    csvReader = csv.reader(csvFile, delimiter=',')
    
    for row in csvReader:
      if regionList is not None:
        if row[1] not in regionList:
          continue
      if row[0] != -1:
        instance.append(row[2])
        if allInOne:
          instanceLabel.append(row[2])
        else:
          instanceLabel.append(row[1] + " " + row[2])
      data.append(row[9:])
      
  if len(data) == 0:
    continue
      
  for ins in instance:
    if ins not in instanceSet and ins != '':
      instanceSet.append(ins)
    #instanceSet = list(set(instance))
    #instanceSet.pop(0)
      
  names = ['runtime', 'cpu usage', 'mem usage', 'price', 'latency']
  target_names = range(0, len(data))
  
  firstIndex = 1
  if topsis:
    firstIndex = 2
  y = range(firstIndex, len(data))
  x_std = StandardScaler().fit_transform(data)
  
  cov_mat = np.cov(x_std.T)
  #print('NumPy covariance matrix: \n%s' %cov_mat)
  
  eig_vals, eig_vecs = np.linalg.eig(cov_mat)
  eig_vals = eig_vals.real
  eig_vecs = eig_vecs.real
  
  eig_pairs = [(np.abs(eig_vals[i]), eig_vecs[:, i], target_names[i]) for i in range(len(eig_vals))]
  
  eig_pairs = sorted(eig_pairs, key=lambda x: x[0], reverse=True)
  
  
  origIdx = sorted(range(len(eig_vals)), key=lambda k: eig_vals[k], reverse=True)
  
  total = sum(eig_vals)
  var_exp = [(i / total)*100 for i in sorted(eig_vals, reverse=True)]
  cum_var_exp = np.cumsum(var_exp)
  ind = np.arange(len(var_exp))
  

  #plt.figure(figsize=(10,8))
  #plt.bar(ind, var_exp, 0.5)
  ##plt.xticks(ind, ['PC %s' %i for i in range(1,len(ind)+1)])
  #plt.xticks(ind, ['PC %d' %(n + 1) for n in range(0,len(names))])
  #plt.ylabel("Explained variance [%]")
  #plt.show()

  #plt.figure(figsize=(10,8))
  #plt.bar(ind, cum_var_exp, 0.5)
  #plt.xticks(ind, ['PC %d' %(n + 1) for n in range(0,len(names))])
  #plt.ylabel("Cumulative explained variance [%]")
  #plt.show()
  
  pca = PCA(n_components=3)
  X_proj = pca.fit_transform(x_std)

  #Plot results
  if not allInOne:
    fig = plt.figure(figsize=(15,12))
    ax = fig.add_subplot(111, projection='3d')
  #ax.plot([loc[0]], [loc[1]], [loc[2]], 'ro')
      
  if not allInOne:
    posLabel = 'User query'
    posColor = 'r'
    if topsis:
      posLabel = 'Ideal vector'
      posColor = 'g'
    ax.scatter(X_proj[0, 0], X_proj[0, 1], X_proj[0, 2], c=posColor, marker='^', label=posLabel, s=50)
    if topsis:
      ax.scatter(X_proj[1, 0], X_proj[1, 1], X_proj[1, 2], c='r', marker='^', label='Anti ideal vector', s=50)
  
  bestVals = 5
  bestValsMarker = '*'
  bestValsSize = 100
  if allInOne:
    bestValsMarker = 'o'
    bestValsSize = 20
  
  colors = ['C0','C1','C2','C3','C4','C5','C6','C7','C8','C9']
  for i, target_name in zip(np.arange(firstIndex, firstIndex + bestVals), instanceLabel[firstIndex:firstIndex + bestVals]):
    ax.scatter(X_proj[i, 0], X_proj[i, 1], X_proj[i, 2], c=colors[instanceSet.index(instance[i])], marker=bestValsMarker, label=target_name, s=bestValsSize)
  for i, target_name in zip(np.arange(firstIndex + bestVals, len(target_names)), instanceLabel[firstIndex + bestVals:]):
    ax.scatter(X_proj[i, 0], X_proj[i, 1], X_proj[i, 2], c=colors[instanceSet.index(instance[i])], marker='o', label=target_name)
    #plt.scatter(X_proj[i, 0], X_proj[i, 1], label=target_name)
    
  if not allInOne:
    ax.set_xlabel("PC1")
    ax.set_ylabel("PC2")
    ax.set_zlabel("PC3")
    plt.legend(loc='best', shadow=False)
    plt.title('Ranking with query vector (Topsis Euclidean)')
    plt.show()
    
if allInOne:
  ax.set_xlabel("PC1")
  ax.set_ylabel("PC2")
  ax.set_zlabel("PC3")
  
  handles, labels = plt.gca().get_legend_handles_labels()
  by_label = OrderedDict(zip(labels, handles))
  plt.legend(by_label.values(), by_label.keys(), loc='best', shadow=False)
  #plt.legend(loc='best', shadow=False)
  plt.title('Vectors of all instances and regions (Topsis)')
  plt.show()
