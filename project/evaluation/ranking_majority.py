import numpy as np
import matplotlib.pyplot as plt
import csv
import codecs
import sys
from os.path import isfile
import os
from glob import glob

import matplotlib.pyplot as plt
import numpy as np

from collections import OrderedDict

if len(sys.argv) < 2:
	print("Usage: python %s <data> [regionList]" % sys.argv[0])
	sys.exit(0)
  
regionList = None
if len(sys.argv) > 2:
  regionList = sys.argv[2].split()

files = glob(sys.argv[1] + "*")

instanceSet = []
majority = []
for i in range(0, 5):
  majority.append({})

for f in files:
  data = []
  instanceLabel = []
  instance = []
  
  with codecs.open(f, "r", encoding='utf-8', errors='ignore') as csvFile:
    csvReader = csv.reader(csvFile, delimiter=',')
    
    nextIdx = 0
    for row in csvReader:
      if row[0] != '-1':
        if regionList is not None:
          if row[1] not in regionList:
            break
        instance.append(row[2])
        instanceLabel.append(row[1] + " " + row[2])
        if row[2] not in majority[nextIdx]:
          majority[nextIdx][row[2]] = 1
        else:
          majority[nextIdx][row[2]] += 1
        nextIdx += 1
        if nextIdx >= 5:
          break
      data.append(row[9:])
      
maxVal = 0
for d in majority:
  if len(d.items()) > maxVal:
    maxVal = len(d.items())

bars = []
for i in range(0, maxVal):
  bars.append([])

finalMaj = [];
for i in range(0, 5):
  print("Ranking position %d majority vote" % (i + 1))
  sortedMaj = sorted([ele for ele in majority[i].items()], key=lambda x: x[1], reverse=True)
  finalMaj.append(sortedMaj)
  nextIndex = 0
  for (instance, count) in sortedMaj:
    print("%s: %d" % (instance, count))
    bars[nextIndex].append(count)
    nextIndex += 1
  while nextIndex < maxVal:
    bars[nextIndex].append(0)
    nextIndex += 1
    
for l in bars:
  print(l)
    
ind = np.arange(5)
    
width = 0.9 / maxVal       # the width of the bars

#fig = plt.figure(figsize=(15,12))
fig, ax = plt.subplots()
fig.set_size_inches(15, 12)
rs = []
count = 0
for l in bars:
  rs.append(ax.bar(ind + width * count, l, width))
  count += 1

# add some text for labels, title and axes ticks
ax.set_ylabel('Count')
ax.set_title('Count of instances on ranking position 1 to 5')
ax.set_xticks(ind + width / 2)
ax.set_xticklabels(('Ranking pos. 1', 'Ranking pos. 2', 'Ranking pos. 3', 'Ranking pos. 4', 'Ranking pos. 5'))

#ax.legend((rects1[0], rects2[0]), ('Men', 'Women'))


def autolabel(rects, idx):
    """
    Attach a text label above each bar displaying its height
    """
    nextIdx = 0
    maxHeight = 0
    for rect in rects:
        height = rect.get_height()
        if height > maxHeight:
          maxHeight = height;
        if height > 0:
          ax.text(rect.get_x() + rect.get_width()/2., height + 0.3,
                  finalMaj[nextIdx][idx][0],
                  ha='center', va='bottom', rotation='vertical')
        nextIdx += 1
    return maxHeight

maxHeight = 0
for i in range(0, len(rs)):
  height = autolabel(rs[i], i)
  if height > maxHeight:
    maxHeight = height

plt.ylim([0, maxHeight + 5])

plt.show()
