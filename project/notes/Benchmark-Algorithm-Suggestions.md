# Benchmark Algorithm Suggestion

Implement in parallel [and sequential]

## CPU Intensive
Matrix multiplication (sufficient)
Inverting matrices
Generating prime numbers
Integer or prime factorization
Ackermann function
Solving some of the Euler problems
CG Algorithms: Fractal generation, Ray tracing...


# Benchmarking tools
glances
vmstat
sysstat
