# Region Selection

## Europe
EU(Ireland) cheapest, ,offers m3 instances <br/> 
EU(Frankfurt) best latency, offers m3 instances<br/>

## America
US East (N. Virginia) cheapest, best latency, offers m3 instances<br/>
US West (N. California)
Canada (Central)<br/>
South America (Sao Paulo)<br/>

## Asia
Asia Pacific (Mumbai) cheapest, best latency but no m3 instances<br/>
Asia Pacific (Tokyo) good latency offers m3 instances<br/>
Asia Pacific (Sydney)<br/>
Asia Pacific (Seoul)<br/>
<br/>
CloudHarmony can test communication performance. Test latency for different regions with vpn from different locations.