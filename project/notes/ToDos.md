# ToDos for 3rd Milestone (working demo, metrics)

* instanceFinder (Manfred)
    * adding price and latency
    * add DataMapper
* CLI Menu - Userinterface (Philipp)
    * add price and latency
    * add DataMapper
* Benchmark (Philipp)
    * only 1 problem size
* Evalation model (Alex)
    * latency
    * benchmark
    * visualization
    * cloud broker

* Presentation Milestone 3 (Clemens)

# ToDos for later Milestones
* Presentation Milestone 4 (Alexander)
* Presentation Milestone 5 (Philipp)
* Interface
* Report

