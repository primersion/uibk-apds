# Instance Selection

## General Purpose
Price in region EU-Ireland (cheapest in europe)

|Type      | vCPU  | RAM(GB)  | ECU      | Storage   | Price(per hour) |
|----------|-------|----------|----------|-----------|-----------------|
|t2.micro  | 1     | 1        | Variable | only EBS  | $0.013           |
|t2.small  | 1     | 2        | Variable | only EBS  | $0.025          |
|t2.medium | 2     | 4        | Variable | only EBS  | $0.05           |
|t2.large  | 2     | 8        | Variable | only EBS  | $0.101          |
|m3.medium | 1     | 3.75     | 3        | 1 x 4 SSD | $0.073          |
|m3.large  | 2     | 7.5      | 6.5      | 1 x 32 SSD| $0.146          |
|m4.large  | 2     | 8        | 6.5      | only EBS  | $0.111          |

## Compute Optimized
|Type      | vCPU  | RAM(GB)  | ECU      | Storage   | Price(per hour) |
|----------|-------|----------|----------|-----------|-----------------|
|c4.large  | 2     | 3.75     | 8        | only EBS  | $0.113          |
|c4.xlarge | 4     | 7.5      | 16       | only EBS  | $0.226          |

## Memory Optimized
|Type      | vCPU  | RAM(GB)  | ECU      | Storage   | Price(per hour) |
|----------|-------|----------|----------|-----------|-----------------|
|r4.large  | 2     | 15.25    | 7        | only EBS  | $0.148          |
|r4.xlarge | 4     | 30.5     | 13.5     | only EBS  | $0.296          |


## [Storage Optimized]
|Type      | vCPU  | RAM(GB)  | ECU      | Storage         | Price(per hour) |
|----------|-------|----------|----------|-----------------|-----------------|
|i3.large  | 2     | 15.25    | 7        | 1 x 475 NVMe SSD| $0.172          |
|i3.xlarge | 4     | 30.5     | 13       | 1 x 950 NVMe SSD| $0.344          |
|d2.xlarge | 4     | 30.5     | 14       | 3 x 2000 HDD    | $0.735          |


#Attention!
From Amazon: Pricing is per instance-hour consumed for each instance, from the time an instance is launched until it is terminated or stopped.
             Each partial instance-hour consumed will be billed as a full hour.

Means we pay at least one full hour just for starting an instance even if the benchmark runs only for few minutes.
