\documentclass[11pt, twocolumn]{article}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[left=2.5cm,right=2.5cm,top=1.5cm,bottom=1.5cm,includeheadfoot]{geometry}
\usepackage{fancyhdr}
\usepackage{amsmath,amsthm,amssymb}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{wasysym}
\usepackage{textcomp}
\usepackage{fancyhdr}
\usepackage{color}
\usepackage{colortbl}
\usepackage{multirow}
\usepackage{pifont}
\usepackage{tabularx}
\usepackage{ifsym}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{amsmath}
\usepackage{caption}
\usepackage{tikz}
\usepackage{url}
\usepackage{listings}
\usepackage{blindtext}
\usepackage{algpseudocode}
\usepackage{lastpage}
\usepackage{csquotes}


\setlength{\parindent}{0pt}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=none,
  language=Java,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=left,
  numberstyle=\footnotesize,
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,    % added this
  tabsize=3
}

\pagestyle{fancy}
\fancyhead{}
\fancyfoot{}
\fancyhead[L]{Advanced Distributed and Parallel Systems}
\fancyhead[R]{Group 3}
\fancyfoot[C]{Page \thepage}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\setlength{\headheight}{10pt}

\fancypagestyle{plain}{%
	\renewcommand{\headrulewidth}{0pt}%
	\fancyhf{}%
	\fancyfoot[C]{Page \thepage}}%


\renewcommand{\arraystretch}{1.5}
\newcommand*\elide{\textup{[\,\dots]}}

\graphicspath{{./img/}}



\begin{document}
\title{Multi Objective Cloud Broker\\\small{Advanced Distributed and Parallel Systems}}
\author{Clemens Degasper, Alexander Jungmann, Manfred Moosleitner, Philipp Stiegernigg}

\maketitle

\section{Introduction}\label{sec:intro}
Consumers and companies often find themselves overwhelmed with the huge variety and offers of cloud providers. The type of instances offered by cloud providers often differ in the number of CPUs, RAM and other hardware. Additionally for some providers the resulting performance is better than for others with the same hardware. Another important factor is the often fluctuating price especially across different regions. These are some of the factors that make it hard to find the right instance for a specific application. At the current time there is no real tool known to us that automatically selects the best instance across different cloud providers and regions. In this paper we present a new approach for selecting an instance across different regions of Amazon EC2 by considering multiple conflicting objectives such as execution time, CPU usage, memory usage, price and latency.

The rest of this report is structured as follows: In Section \ref{sec:relwork} we give a brief overview of state-of-the-art methods. In Section \ref{sec:ec2} we give basic information about Amazon EC2 and which regions and service-types we selected. This is followed up with details about our implementation in Section \ref{sec:impl} and in Section \ref{sec:eval} the evaluation of our system is presented. The report ends with a conclusion in Section \ref{sec:disc}.

\section{Related Work}\label{sec:relwork}
In the past few years several methods to deal with the above mentioned problems have emerged. For example one team proposed an approach based on machine learning and using a feedback loop to improve accuracy over time \cite{7502858}. Another proposed methodology makes use of QoS-history of cloud providers and ranks this data by the use of multi-criteria decision analysis (MCDA/MCDM) \cite{6531879}. Others compared classical MCDM methods to rank the possible alternatives like price per hour and benchmarked performance metrics for CPU, memory and I/O \cite{6468246}.

\section{Amazon EC2}\label{sec:ec2}
Amazon offers their EC2 services in data-centers distributed over multiple locations around the world, and per data-center a broad range of different instance-types are available. In this section we describe which region and instance-types we selected and which combinations we have chosen.

\subsection{Region Selection}\label{sec:regselect}
We initially selected 9 Amazon regions for our project. Later we decided, because of our limited budget, to use only four and evaluate them in more detail. The regions we selected include North Virginia, Frankfurt, Tokyo and Sydney, as illustrated in Figure \ref{fig:regions} \footnote{Image taken from http://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/setting-region.html and adapted}.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.5\textwidth]{regions.png}
  \caption{Regions we selected for our project marked in green.}
  \label{fig:regions}
\end{figure}

\subsection{Instance Selection}\label{sec:instselect}
``Amazon EC2 provides a wide selection of instance types \textelp{} with varying combinations of CPU, memory, storage, and networking capacity \textelp{}''~\cite{instanceTypes}. We selected 10 instances trying to vary the available CPU's, memory and ECU's, while still fitting into our budget. The selection includes general purpose instances (T2 and M3), compute optimized instances (C4) and memory optimized instances (R4). Initially we also selected M4, but we later dropped it, because it wasn't supported by all regions. The final selection is illustrated in Table \ref{tab:instances}.

\begin{table}
  \centering
  \begin{tabular}{|c | c | c | c|}
  \hline
  \textbf{Type} & \textbf{vCpu} & \textbf{RAM} & \textbf{ECU}\\
  \hline
  t2.micro & 1 & 1 & Variable\\
  t2.small & 1 & 2 & Variable\\
  t2.medium & 2 & 4 & Variable\\
  t2.large & 2 & 8 & Variable\\
  \hline
  m3.medium & 1 & 3.75 & 3\\
  m3.large & 2 & 7.5 & 6.5\\
  \hline
  c4.large & 2 & 3.75 & 8\\
  c4.xlarge & 4 & 7.5 & 16\\
  \hline
  r4.large & 2 & 15.25 & 7\\
  r4.xlarge & 4 & 30.5 & 13.5\\
  \hline
  \end{tabular}
  \caption{Instances we have selected.}
  \label{tab:instances}
\end{table}

\section{Implementation}\label{sec:impl}

In this section we will discuss the details of the system we have developed. We will start by explaining the architecture of our cloud broker and all of its components in more detail.

\subsection{Architecture}\label{sec:arch}

The overall structure of our system is shown in Figure \ref{fig:broker_arch}.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.5\textwidth]{broker_arch.png}
  \caption{Architecture of our cloud broker which was adapted from \protect\cite{lin16}.}
  \label{fig:broker_arch}
\end{figure}

First of all we need information about the different instance types that are provided. We retrieve this information from Amazon and store it in our database as can be seen on the bottom right of Figure \ref{fig:broker_arch}.

Next we need to run a benchmark on the available instances to have useful data we can run our cloud broker on. Therefore we start our benchmark on all instances and save the results to S3 upon completion. The results are then retrieved from S3 and stored into our database automatically. The benchmarking step is explained in more detail in Section \ref{sec:bench}.

Starting from the left a consumer can now request an instance for an application by entering a vector with preferences regarding runtime, CPU usage, memory usage, price and latency. The user can enter values between 0 and 10, where 0 means not important at all and 10 means very important. The cloud broker then tries to find the optimal instances by ranking the available benchmark data with respect to the entered vector. The result of the previous step is provided to the user to select and start one of the instances.

\subsection{Benchmark}\label{sec:bench}
For the benchmark we have used a simple program\footnote{Git repository with benchmark: https://gitlab.com/primersion/cloud-broker-benchmark} performing matrix multiplication in parallel using Open MPI. We run the matrix multiplication 5 times with an input size for the matrix of 2000x2000. We have created a Java program using the Java AWS SDK, that starts all instances in each of the selected regions. When an instance has finished starting up, a run script is executed that automatically installs all the software that is necessary and downloads the benchmark from the git repository and starts it. After completion of the benchmark the results are written to S3 . The Java program that started the instances periodically queries S3 for available results of the benchmark and writes them to a local database. Once all results have been collected the program
shuts down all instances.

\subsection{Cloud Broker}\label{sec:cloudbroker}

\subsubsection{Vector Space Model}
As described in \cite{Manning:2008:IIR:1394399}, the vector space model is a well known and wide used model in information retrieval. In this model each document is represented by a sparse vector, which's length is the number of single terms found in the entire collection. The entries of the vectors carry the relevance of the corresponding term for the document. When a user searches in the document collection, the query-vector can be compared with the document-vectors by calculating the distance or the angle between them. In IR it is common to use cosine similarity, which is calculated by taking the inner-product between the document- and the query-vector and dividing it by the product of the euclidean length, formally:
\begin{align*}\text{cosine similarity(d, q)} = \frac{\vec{d} \cdot \vec{q}}{|\vec{q}| \cdot |\vec{d}|}\end{align*}
Our idea was to build an n-dimensional vector space with each criteria being one dimension, and use the performance data from the benchmark, prices from the price list and the latency to populate the instance-vectors. After applying vector-normalization, we can compute cosine similarity between each instance- and query-vector, resulting in a ranking of all instances with respect to the users needs.

\subsubsection{TOPSIS}\label{sec:topsis}
The closest approach to ours in related work is a method called ``Technique for Order Preference by Similarity to Ideal Solution'', short TOPSIS, as described in TOPSIS \cite{OPRICOVIC2004445}, firstly, the input of the user is tranformed into weightings for each criteria, which are applied to the collected data. The next step is to identify the ideal/anti-ideal by taking the best/worst values of each criteria respectively. With this the euclidean distance from each alternative to ideal (di) and anti-ideal (dai) is calculated and the relative closeness for each alternative is computed as follows
\begin{align*}\text{rel. closeness} = \frac{\text{dai}}{\text{di}+\text{dai}}\end{align*}
and this again creates a ranking of all the alternatives by their relative closeness. We implemented TOPSIS in means to compare our approach and implemented a variation of TOPSIS by using cosine similarity instead of euclidean distance.

\subsection{Command Line Interface}\label{sec:cli}
In order to give the user the possibility to interact with our system, we have designed a command
line interface which is simple to use but still provides access to all important features of
the cloud broker.

Once the application was launched it shows information about the startup phase of the cloud broker
including the status of the Amazon EC2 client creation and fetching of all AMIs. At the time the
cloud broker has finished all preparations it checks whether the current price list on disk is up-to-date and otherwise downloads a new one from Amazon. After that the cloud broker tries to ping an instance in each of the Amazon regions to acquire the latency from the current location. Next the user is prompted with a choice to reuse the old
benchmark results if local data was found on the machine. If no data was found or the user
declines, the command line will show the current progress of the benchmark evaluation until
it has finished. In either case the user has to specify his or her application needs to represent
the preferences as described in section \ref{sec:arch}. Afterwards the user is presented with a ranking of all regions as seen in table
\ref{tab:ranking}. Finally the user can enter the id of the preferred instance which is then launched.

\begin{table}[]
\centering
\begin{tabular}{|l|l|l|}
\hline
\textbf{ID} & \textbf{Region} & \textbf{Instance} \\ \hline
000           & ap-southeast-2  & t2.medium         \\ \hline
001           & ap-northeast-1  & t2.medium         \\ \hline
002           & ap-northeast-1  & t2.large          \\ \hline
003           & ap-southeast-2  & t2.large          \\ \hline
004           & ap-northeast-1  & r4.large          \\ \hline
\end{tabular}
\caption{Instance ranking presented to user}
\label{tab:ranking}
\end{table}

\section{Evaluation}\label{sec:eval}
We wanted to evaluate several things, first how latency changes over time, for which we collected data in a time span of one week. As can be seen in figure \ref{fig:pingoneweek}, the data we recorded suggests that a fluctuation in latency to the different regions is to be expected on a regular basis.
\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.5\textwidth]{ping_one_week.png}
  \caption{Latency-Benchmark, runtime 1 week.}
  \label{fig:pingoneweek}
\end{figure}
We also wanted to see the behavior of the performances of the instances over time by collecting benchmark data over 24 hours for the selected instances, as already stated in section \ref{sec:ec2}. Figure \ref{fig:exectime} shows the behavior of the instances expressed in measured runtime of running the benchmark with the same problem size.
\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.5\textwidth]{exec_time.png}
  \caption{Overview of differing behavior across regions and instance-types.}
  \label{fig:exectime}
\end{figure}
As prices are important, we also looked at price variation across regions and instance-types, as can be seen in figure \ref{fig:prices}. The data clearly shows that some regions are more costly than others.
\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.5\textwidth]{instance_prices2.png}
  \caption{Overview of price variations per instance-type and regions.}
  \label{fig:prices}
\end{figure}

Finally we wanted to compare our approach with TOPSIS and its variant with cosine similarity.
Figure \ref{fig:cosineBenchmark} shows several benchmark result which were acquired
by running our benchmark suit periodically once per hour for a whole day on all
selected instances and regions. The plot shows the mapped benchmark results reduced
by a principal component analysis. Each color represents one instance type and
every point corresponds to a vector used by the cosine model to compute the
similarity to the user vector. It can be seen that for every instance we have
sparse and widespread clusters and overlapping for some instance types.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.5\textwidth]{cosine_plot.png}
  \caption{Unweighted benchmark results used by the cosine model.}
  \label{fig:cosineBenchmark}
\end{figure}

In comparison figure \ref{fig:topsisBenchmark} shows the same benchmark data
weighted by the user vector as it is done in the TOPSIS model. The clusters
for every instance type are much denser and there is less overlapping in contrast
to the unweighted data. This let us believe that the TOPSIS model will provide
much better results as the simple cosine model. However a real performance evaluation of
the cloud broker itself is difficult since it would require a user study
which was out of the scope of this project.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.5\textwidth]{topsis_plot.png}
  \caption{Benchmark results weighted by user vector used by the TOPSIS model.}
  \label{fig:topsisBenchmark}
\end{figure}

Difference in ranking can also be seen by the direct comparison of the top 5 ranked instances for each model, displayed in table \ref{tab:comparison}.

\begin{table}[]
\centering
\begin{tabular}{|l|l|l|}
\hline
\textbf{cos} & \textbf{TOPSIS L2} & \textbf{TOPSIS cos} \\ \hline
us-east-1	& us-east-1		& us-east-1 \\ 
t2.large		& t2.medium	& r4.xlarge \\ \hline
us-east1 	& us-east-1		& eu-central-1 \\
t2.medium	& t2.large		& c4.xlarge \\  \hline
us-east-1	& eu-central-1	& us-east-1\\
c4.xlarge	& t2.large		& c4.xlarge \\ \hline
us-east-1	& eu-central-1	& eu-central-1 \\
r4.xlarge	& c4.large		& t2.medium \\ \hline
us-east-1	& us-east-1		& us-east-1 \\
r4.large		& c4.large		& t2.large \\ \hline
\end{tabular}
\caption{Comparison of the 3 approaches.}
\label{tab:comparison}
\end{table}

\section{Conclusion}\label{sec:disc}
In this report we documented our experiment on trying to use methods from information retrieval in the context of a cloud broker and compared it with multi-criteria decision methods. Although all 3 approaches show a different result, the top 5 ranks are made up from the same instance-types, making it possible to compare the results. The 3 models seem to have a bias on data-centers in the US and in Europe, which is most likely due to lower prices in US and European data-centers, making those a good value for money choice. With a higher budget it would have been possible to not only collect data from more EC2 regions, but it would have opened the possibility to collect data from other cloud-providers, further enhancing the range of options for the cloud-broker.



\bibliography{bibliography}
\bibliographystyle{unsrt}

\end{document}
