package com.amazonaws.samples;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class S3Example {

	public static final String BUCKET_NAME = "java-s3-example-alexander-bucket";

    public static void main(String[] args) throws IOException {

    	 /*****************Load the credentials****************/

        AWSCredentials credentials = null;
        try {
            credentials = new ProfileCredentialsProvider("default").getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (/home/dragi/.aws/credentials), and is in valid format.",
                    e);
        }

        /*****************Set the availability zones****************/
        AmazonS3 s3 = AmazonS3ClientBuilder.standard()
        		.withCredentials(new AWSStaticCredentialsProvider(credentials))
        		.withRegion(Regions.EU_CENTRAL_1)
        		.build();

        String bucketName = BUCKET_NAME;
        String fileName = "MyFile.txt";

        /*****************Create bucket - NOTE: the bucked should unique for all Amazon users****************/
        try {
            /*
             * List all available buckets in your account
             */
        	boolean bucketExists = false;
            System.out.println("List the buckets");
            for (Bucket bucket : s3.listBuckets()) {
            	if(bucket.getName().equals(BUCKET_NAME))
            		bucketExists = true;
                System.out.println(" ->> " + bucket.getName());
            }
            System.out.println();

            /**************** Create bucket if it doesn't already exist **************/
            if(!bucketExists){
	            System.out.println("Creating bucket " + bucketName + "\n");
	            s3.createBucket(bucketName);
            }

            /**********Upload new object in the bucket if it doesn't already exist**********/
            if(!s3.doesObjectExist(bucketName, fileName)){
	            System.out.println("Uploading a new file to the Amazon S3 \n");
	            s3.putObject(new PutObjectRequest(bucketName, fileName, createSampleFile()));
            } else{
            	System.out.println("File " + fileName + " already exists");
            }


            /*****************List all files in the bucket****************/
            System.out.println("Listing objects");
            ObjectListing objectListing = s3.listObjects(new ListObjectsRequest()
                    .withBucketName(bucketName));
            for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
                System.out.println(" - " + objectSummary.getKey() + "  " +
                                   "(size = " + objectSummary.getSize() + ")");
            }
            System.out.println();


            /*****************Download file from bucket****************/
            System.out.println("Downloading file " + fileName + " from bucket");
            S3Object object = s3.getObject(bucketName, fileName);
            System.out.println("Content-Type: "  + object.getObjectMetadata().getContentType());
            displayTextInputStream(object.getObjectContent());

            /*****************Renaming file****************/
            System.out.println("Renaming file");
            String newFileName = "RenamedFile.txt";
            CopyObjectRequest copyObjRequest = new CopyObjectRequest(bucketName,
					fileName, bucketName, newFileName);
	        s3.copyObject(copyObjRequest);
	        s3.deleteObject(new DeleteObjectRequest(bucketName, fileName));

            /*****************Delete file from bucket****************/
            System.out.println("Deleting file " + newFileName + " from bucket");
            s3.deleteObject(bucketName, newFileName);

            /*****************Empty bucket and delete bucket****************/
            System.out.println("Emptying bucket");
            objectListing = s3.listObjects(new ListObjectsRequest()
                    .withBucketName(bucketName));
            for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
                s3.deleteObject(bucketName, objectSummary.getKey());
            }
            System.out.println("Deleting bucket " + bucketName);
            s3.deleteBucket(bucketName);

        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it "
                    + "to Amazon S3, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with S3, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
    }

    /**
     * Creates a temporary file with text data to demonstrate uploading a file
     * to Amazon S3. This code was utilized from the Amazon S3 wizard.
     *
     * @return A newly created temporary file with text data.
     *
     * @throws IOException
     */
    private static File createSampleFile() throws IOException {
        File file = File.createTempFile("aws-java-sdk-proseminar", ".txt");
        file.deleteOnExit();

        Writer writer = new OutputStreamWriter(new FileOutputStream(file));
        writer.write("This is a test file for the\n");
        writer.write("Proseminar in APDS\n");
        writer.close();

        return file;
    }

    private static void displayTextInputStream(InputStream input)
    	    throws IOException {
    	// Read one text line at a time and display.
        BufferedReader reader = new BufferedReader(new
        		InputStreamReader(input));
        while (true) {
            String line = reader.readLine();
            if (line == null) break;

            System.out.println("    " + line);
        }
        System.out.println();
    }
}
