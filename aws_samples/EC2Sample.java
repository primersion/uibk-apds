package com.amazonaws.samples;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.json.simple.parser.ParseException;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.AttachVolumeRequest;
import com.amazonaws.services.ec2.model.AttachVolumeResult;
import com.amazonaws.services.ec2.model.AuthorizeSecurityGroupIngressRequest;
import com.amazonaws.services.ec2.model.AvailabilityZone;
import com.amazonaws.services.ec2.model.CreateKeyPairRequest;
import com.amazonaws.services.ec2.model.CreateKeyPairResult;
import com.amazonaws.services.ec2.model.CreateSecurityGroupRequest;
import com.amazonaws.services.ec2.model.CreateSecurityGroupResult;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.CreateVolumeRequest;
import com.amazonaws.services.ec2.model.CreateVolumeResult;
import com.amazonaws.services.ec2.model.DescribeAvailabilityZonesResult;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeKeyPairsRequest;
import com.amazonaws.services.ec2.model.DescribeKeyPairsResult;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsRequest;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsResult;
import com.amazonaws.services.ec2.model.DescribeVolumesRequest;
import com.amazonaws.services.ec2.model.DescribeVolumesResult;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceStateChange;
import com.amazonaws.services.ec2.model.IpPermission;
import com.amazonaws.services.ec2.model.IpRange;
import com.amazonaws.services.ec2.model.KeyPair;
import com.amazonaws.services.ec2.model.KeyPairInfo;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.SecurityGroup;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;
import com.amazonaws.services.ec2.model.TerminateInstancesResult;
import com.amazonaws.services.ec2.model.Volume;

public class EC2Sample {

	public static final String KEY_DIR = "/home/alexander/";
	public static final String SECURITY_GROUP_NAME = "JavaSecurityGroupExample";
	public static final String KEY_PAIR_NAME = "JavaKeyPairExample";
	public static final String VOLUME_NAME = "JavaVolumeExample";
	
	public static final String AMI_AMAZON_LINUX = "ami-af0fc0c0";

	public static final int THREAD_SLEEP_TIME = 5000;

	static KeyPair keyPair;
	static AmazonEC2 ec2;

	/**
	 * @param args
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws IOException, ParseException, InterruptedException {

		/***************** Load the credentials ****************/

		AWSCredentials credentials = null;
		try {
			credentials = new ProfileCredentialsProvider("default").getCredentials();
		} catch (Exception e) {
			throw new AmazonClientException("Cannot load the credentials from the credential profiles file. "
					+ "Please make sure that your credentials file is at the correct "
					+ "location (/home/user/.aws/credentials), and is in valid format.", e);
		}

		/***************** Create EC2 Client ******************/
		ec2 = AmazonEC2ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials))
				.withRegion(Regions.EU_CENTRAL_1).build();

		/***************** List the availability zones ****************/
		DescribeAvailabilityZonesResult availabilityZonesResult = ec2.describeAvailabilityZones();
		List<AvailabilityZone> zones = availabilityZonesResult.getAvailabilityZones();
		for (int i = 0; i < availabilityZonesResult.getAvailabilityZones().size(); i++)
			System.out.println(zones.get(i));

		if (zones == null || zones.size() == 0) {
			System.out.println("Could not obtain any availability Zone");
			System.exit(0);
		}

		/*****************
		 * Set filter and list available AMIs/VMIs
		 ****************/
		// DescribeImagesRequest imageRequest = new
		// DescribeImagesRequest().withFilters(new LinkedList<Filter>());
		// imageRequest.getFilters().add(new
		// Filter().withName("platform").withValues("windows"));
		// imageRequest.getFilters().add(new
		// Filter().withName("image-type").withValues("machine"));
		// List<Image> images = ec2.describeImages(imageRequest).getImages();
		// System.out.println("Image Request Result: " + images.size());

		/***************** Create new security group ****************/
		List<SecurityGroup> groups = getSecurityGroups();
		
		String gid = null;
		boolean newGroup = false;
		for(SecurityGroup g : groups){
			if(g != null && g.getGroupName().equals(SECURITY_GROUP_NAME)){
				gid = g.getGroupId();
				System.out.println("Using existing security group " + g.getGroupName());
				break;
			}
		}
		if(gid == null){
			System.out.println("Creating new security group");
			gid = createSecurityGroup();
			newGroup = true;
		}

		List<String> groupNames = new ArrayList<String>();
		groupNames.add(gid);

		if(newGroup){
			/***************** Set incoming traffic policy ****************/
	
			String ipAddress = getMyIp();
			System.out.println("My IP: " + ipAddress);
	
			IpRange ipRange = new IpRange().withCidrIp(ipAddress + "/32");
	
			IpPermission ipPermission = new IpPermission();
			ipPermission.withIpv4Ranges(Arrays.asList(new IpRange[] { ipRange })).withIpProtocol("tcp").withFromPort(22)
					.withToPort(22);
			IpPermission ipPermission2 = new IpPermission();
			ipPermission2.withIpv4Ranges(Arrays.asList(new IpRange[] { ipRange })).withIpProtocol("tcp").withFromPort(80)
					.withToPort(80);
	
			/***************** Authorize ingress traffic ****************/
			try {
				AuthorizeSecurityGroupIngressRequest ingressRequest = new AuthorizeSecurityGroupIngressRequest();
	
				ingressRequest.withGroupId(gid).withIpPermissions(ipPermission, ipPermission2);
				ec2.authorizeSecurityGroupIngress(ingressRequest);
				System.out.println(String.format("Ingress port authroized: [%s]", ipPermission.toString()));
				System.out.println(String.format("Ingress port authroized: [%s]", ipPermission2.toString()));
			} catch (AmazonServiceException ase) {
				System.out.println(ase.getMessage());
			}
		}

		String keyName = null;
		List<KeyPairInfo> keyPairs = getKeyPairs();
		for(KeyPairInfo k : keyPairs){
			if(k != null && k.getKeyName().equals(KEY_PAIR_NAME)){
				System.out.println("Using existing keypair " + k.getKeyName());
				keyName = k.getKeyName();
				break;
			}
		}
		
		if(keyName == null){
			/***************** Create a key for the VM ****************/
			CreateKeyPairRequest newKReq = new CreateKeyPairRequest();
			newKReq.setKeyName(KEY_PAIR_NAME);
			CreateKeyPairResult kresult = ec2.createKeyPair(newKReq);
			keyPair = kresult.getKeyPair();
			System.out.println("Key for the VM was created  = " + keyPair.getKeyName() + "\nthe fingerprint is="
					+ keyPair.getKeyFingerprint() + "\nthe material is= \n" + keyPair.getKeyMaterial());
	
			/***************** Store the key in a .pem file ****************/
			keyName = keyPair.getKeyName();
			String fileName = KEY_DIR + keyName + ".pem";
			File distFile = new File(fileName);
			BufferedReader bufferedReader = new BufferedReader(new StringReader(keyPair.getKeyMaterial()));
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(distFile));
			char buf[] = new char[1024];
			int len;
			while ((len = bufferedReader.read(buf)) != -1) {
				bufferedWriter.write(buf, 0, len);
			}
			bufferedWriter.flush();
			bufferedReader.close();
			bufferedWriter.close();
		}

		/***************** Start a given free tier instance ****************/
		int mIC = 1; // minimum number instances to launch
		int maxIC = 1; // maximum number instances to launch
		RunInstancesRequest rir = new RunInstancesRequest(AMI_AMAZON_LINUX, mIC, maxIC);
		rir.setInstanceType("t2.micro");
		rir.setKeyName(keyName);
		rir.withSecurityGroupIds(groupNames);

		/************* Run instance and wait until it is created ************/
		RunInstancesResult result = ec2.runInstances(rir);
		List<Instance> instances = result.getReservation().getInstances();

		List<String> instanceIds = new LinkedList<String>();
		for (Instance ins : instances) {
			instanceIds.add(ins.getInstanceId());
			System.out.println("New instance has been created: " + ins.getInstanceId());
		}

		/*****************
		 * Wait until machine is in running state
		 ****************/
		instances = waitUntilInstancesRunning(instanceIds, instances);

		/************** Set machine tag ******************/
		Instance instance = instances.get(0);
		System.out.println("Setting machine tag");
		ArrayList<Tag> machineTags = new ArrayList<Tag>();
		machineTags.add(new Tag("Name", "Machine from eclipse"));
		CreateTagsRequest machineTagReq = new CreateTagsRequest().withTags(machineTags)
				.withResources(instance.getInstanceId());
		ec2.createTags(machineTagReq);

		
		List<Volume> existingVolumes = requestVolumes(null);
		Volume volume = null;
		for(Volume v : existingVolumes){
			if(v != null){
				for(Tag t : v.getTags()){
					if(t.getValue().equals(VOLUME_NAME)){
						System.out.println("Using existing volume " + v.getVolumeId());
						volume = v;
						break;
					}
				}
			}
		}
		
		if(volume == null){
			/***************** Create EBS volume for the instance ****************/
			String availZone = instance.getPlacement().getAvailabilityZone();
			System.out.println("Creating the volume begins...");
			CreateVolumeRequest creq = new CreateVolumeRequest(5, availZone);
			CreateVolumeResult cres = ec2.createVolume(creq);
			volume = cres.getVolume();
	
			// Create the list of tags we want to create
			System.out.println("Setting the tags to the volume...");
			ArrayList<Tag> instanceTags = new ArrayList<Tag>();
			instanceTags.add(new Tag("Name", VOLUME_NAME));
			CreateTagsRequest createTagsRequest = new CreateTagsRequest().withTags(instanceTags)
					.withResources(volume.getVolumeId());
			ec2.createTags(createTagsRequest);
		}

		/*****************
		 * Wait until volume is in available state
		 ***************/
		List<String> volumeIds = new LinkedList<String>();
		volumeIds.add(volume.getVolumeId());
		List<Volume> volumes = new LinkedList<Volume>();
		volumes.add(volume);

		volumes = waitUntilVolumesAvailable(volumeIds, volumes);

		System.out.println("Attaching the volume to the instance....");
		AttachVolumeRequest areq = new AttachVolumeRequest(volume.getVolumeId(), instance.getInstanceId(),
				"/dev/sdh");
		AttachVolumeResult ares = ec2.attachVolume(areq);
		System.out.println("Attachment: " + ares.getAttachment());

		/*****************
		 * Print the public IP and DNS of the instance
		 ****************/

		for (Instance i : instances) {
			System.out.println("Public IP: " + i.getPublicIpAddress());
			System.out.println("DNS: " + i.getPublicDnsName());
		}

		/*****************
		 * Terminate the instance after given time period
		 ****************/

		System.out.println("Shutting down in 30 seconds");
		Thread.sleep(30 * 1000);
		requestTerminate(instanceIds);
	}

	public static String getMyIp() throws IOException {
		URL whatismyip = new URL("http://checkip.amazonaws.com");
		BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
		return in.readLine();
	}

	public static List<Instance> waitUntilInstancesRunning(List<String> instanceIds, List<Instance> instances)
			throws InterruptedException {
		while (!instancesRunning(instances)) {
			Thread.sleep(THREAD_SLEEP_TIME);
			instances = requestUpdatedInstance(instanceIds, null);
		}
		return instances;
	}

	public static boolean instancesRunning(List<Instance> instances) {
		for (Instance i : instances) {
			if (!i.getState().getName().equals("running")) {
				System.out.println(
						"Waiting for instance " + i.getInstanceId() + " to start up. State: " + i.getState().getName());
				return false;
			}
		}
		return true;
	}

	public static List<Volume> waitUntilVolumesAvailable(List<String> volumeIds, List<Volume> volumes)
			throws InterruptedException {
		while (!volumesAvailable(volumes)) {
			Thread.sleep(THREAD_SLEEP_TIME);
			volumes = requestVolumes(volumeIds);
		}
		return volumes;
	}

	public static boolean volumesAvailable(List<Volume> volumes) {
		for (Volume v : volumes) {
			if (!v.getState().equals("available")) {
				System.out.println("Waiting for volume to be available: " + v.getVolumeId());
				return false;
			}
		}
		return true;
	}

	public static void requestTerminate(List<String> instanceIds) {
		System.out.println("Shutting down instances");
		TerminateInstancesRequest terminateRequest = new TerminateInstancesRequest(instanceIds);
		TerminateInstancesResult terminateResult = ec2.terminateInstances(terminateRequest);
		for (InstanceStateChange s : terminateResult.getTerminatingInstances())
			System.out.println(
					"Stutdown initiated for instance id: " + s.getInstanceId() + " State: " + s.getCurrentState());
	}

	/**
	 * Requests an updated lists of all instances, with an optional filter
	 * 
	 * @param instanceId
	 * @param filters
	 * @return
	 */
	public static List<Instance> requestUpdatedInstance(List<String> instanceId, List<Filter> filters) {
		DescribeInstancesRequest request = new DescribeInstancesRequest();
		request.setInstanceIds(instanceId);

		if (filters != null)
			request.setFilters(filters);

		List<Reservation> list = ec2.describeInstances(request).getReservations();
		List<Instance> instances = new LinkedList<Instance>();
		for (Reservation res : list)
			for (Instance ins : res.getInstances())
				instances.add(ins);
		return instances;
	}

	/**
	 * Requests an updated list of volumes
	 * 
	 * @param volumeId
	 * @return
	 */
	public static List<Volume> requestVolumes(List<String> volumeIds) {
		DescribeVolumesRequest request = null;
		if(volumeIds != null)
			request = new DescribeVolumesRequest(volumeIds);
		else
			request = new DescribeVolumesRequest();
		DescribeVolumesResult result = ec2.describeVolumes(request);
		return result.getVolumes();
	}

	public static List<SecurityGroup> getSecurityGroups() {
        DescribeSecurityGroupsRequest request = new DescribeSecurityGroupsRequest();
       
        DescribeSecurityGroupsResult result = ec2.describeSecurityGroups(request);
        return result.getSecurityGroups();
    }
	
	public static String createSecurityGroup(){
		try {
			CreateSecurityGroupRequest csgr = new CreateSecurityGroupRequest();
			csgr.withGroupName(SECURITY_GROUP_NAME).withDescription("Proseminar security group");
			CreateSecurityGroupResult resultsc = ec2.createSecurityGroup(csgr);
			System.out.println(String.format("Security group created: [%s]", resultsc.getGroupId()));
			return resultsc.getGroupId();
		} catch (AmazonServiceException ase) {
			// Likely this means that the group is already created, so ignore.
			System.out.println(ase.getMessage());
		}
		return null;
	}
	
	public static List<KeyPairInfo> getKeyPairs(){
		DescribeKeyPairsRequest request = new DescribeKeyPairsRequest();
		DescribeKeyPairsResult result = ec2.describeKeyPairs(request);
		return result.getKeyPairs();
	}
}
