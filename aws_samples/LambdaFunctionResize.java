package com.amazonaws.lambda.demo;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;

public class LambdaFunctonResize implements RequestHandler<S3Event, String> {
	
	public static final String BUCKET_NAME = "lambda-example-proseminar-alexander";
	
    public String handleRequest(S3Event event, Context context) {
    	AmazonS3 s3 = new AmazonS3Client(new DefaultAWSCredentialsProviderChain());
    	
    	String key = null;
    	try{
    		key = event.getRecords().get(0).getS3().getObject().getKey();
    	} catch(Exception e){
    		return "Error occured while trying to get object";
    	}
    	
        S3Object object = s3.getObject(BUCKET_NAME, key);
        byte[] origImg = null;
        try {
			origImg = IOUtils.toByteArray(object.getObjectContent());
		} catch (IOException e) {
			
		}
        
        if(origImg == null)
        	return "Error getting image from bucket";
        byte[] scaledImg = scaleImage(origImg, 0.5f, 0.5f);
        if(scaledImg == null)
        	return "Error scaling image";
        
        InputStream is = new ByteArrayInputStream(scaledImg);
        ObjectMetadata meta = object.getObjectMetadata();
        try{
        	meta.setContentLength(is.available());
        } catch(Exception e){
        	return "Error setting content length";
        }
        s3.putObject(BUCKET_NAME, "SCALED_" + key, is, object.getObjectMetadata());
    	
    	return "Success";
    }
    
    public byte[] scaleImage(byte[] fileData, float widthF, float heightF) {
        ByteArrayInputStream in = new ByteArrayInputStream(fileData);
        try {
            BufferedImage img = ImageIO.read(in);
            if(heightF <= 0) {
            	return null;
            }
            if(widthF <= 0) {
            	return null;
            }
            int newWidth = (int) (img.getWidth() * widthF);
            int newHeight = (int) (img.getHeight() * heightF);
            Image scaledImage = img.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
            BufferedImage imageBuff = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
            imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(0,0,0), null);

            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            ImageIO.write(imageBuff, "jpg", buffer);

            return buffer.toByteArray();
        } catch (IOException e) {
            return null;
        }
    }
}
