#!/bin/bash

if [ "$#" -eq 0 ] ; then
  echo "Usage: $0 <output-file.json>"
  echo "Example: $0 result.json"
  exit 1
fi

cd algorithms/
cmake .
make all
./run.sh
cd ..
./analyse.sh algorithms > $1
