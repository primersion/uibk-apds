### Prerequisites

- C compiler with openmpi
- CMake
- Python

### Algorithms included

- Matrix multiplication
- Sieve of eratosthenes
- Bubble sort

### Running the algorithms

- Run 'chmod +x ./run-and-analyse.sh' to make the script runnable.
- Then simply run './run-and-analyse.sh output-file.json' to compile and run the algorithms and produce a .json output file containing the results.

**Important Note:** The input values for the algorithms may need to be tuned to work 
properly, depending on the available memory. To do so open up 'algorithms/run.sh' and 
edit SIEVE_SIZE, MATRIX_SIZE and BUBBLE_SORT_SIZE. For the sieve i used 150000000 on 
the local machine, 40000000 on the vm with 1GB of RAM and 100000000 on the vm with 
2GB of RAM.
