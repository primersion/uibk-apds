/*
 * Determine prime numbers using the sieve of Eratosthenes.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*
 * Finds prime numbers and marks it in the sieve.
 * 0 = Prime number, 1 = Not prime number
 */
void sieve_of_eratosthenes(int n, int *primes) {
  int m = (int)sqrt((double)n);
  for (int i = 2; i <= m; i++) {
    if (!primes[i]) {
      for (int j = i * i; j <= n; j += i)
        if (!primes[j]) {
          primes[j] = 1;
        }
    }
  }
}

/*
 * Algorithm.
 */
void init(int n) {
  if (n < 2)
    return;

  struct timespec start, end;
  
  // Initialize sieve.
  int *primes = (int *)calloc(n + 1, sizeof(int));
  primes[0] = 1;
  primes[1] = 1;

  clock_gettime(CLOCK_MONOTONIC, &start);
  
  sieve_of_eratosthenes(n, primes);

  clock_gettime(CLOCK_MONOTONIC, &end);
  double exec_time = (end.tv_sec - start.tv_sec);
  exec_time += (end.tv_nsec - start.tv_nsec) / 1000000000.0;

  // print as csv (call it parallel to support the analyse script even though
  // it
  // is single threaded)
  // algo | execTime [s] | problemSize | numOfUsedThreads | comment
  printf("sieve,%.3f,%d,%d,none\n", exec_time, n, 1);

  free(primes);
}

int main(int argc, char *argv[]) {
  if (argc < 2)
    return EXIT_FAILURE;

  int problemSize = atoi(argv[1]);
  init(problemSize);

  return EXIT_SUCCESS;
}
