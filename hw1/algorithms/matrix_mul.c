/*
 * Sequential matrix multiplication of two NxN matrices.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "matrix_util.h"

/*
 * Multiplies two matrices.
 */
void matrix_mul(int N, double **A, double **B, double **C) {
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      for (int k = 0; k < N; k++)
        C[i][j] += A[i][k] * B[k][j];
}

/*
 * Main program.
 */
int main(int argc, char *argv[]) {
  if (argc < 2)
    return EXIT_FAILURE;

  int problemSize = atoi(argv[1]);
  struct timespec start, end;

  srand(time(NULL));

  double **A, **B, **C;

  A = allocateMat(problemSize, problemSize);
  B = allocateMat(problemSize, problemSize);
  C = allocateMat(problemSize, problemSize);

  initMat(A, problemSize, problemSize);
  initMat(B, problemSize, problemSize);

  clock_gettime(CLOCK_MONOTONIC, &start);

  matrix_mul(problemSize, A, B, C);

  clock_gettime(CLOCK_MONOTONIC, &end);
  double exec_time = (end.tv_sec - start.tv_sec);
  exec_time += (end.tv_nsec - start.tv_nsec) / 1000000000.0;

  // print as csv (call it parallel to support the analyse script even though
  // it
  // is single threaded)
  // algo | execTime [s] | problemSize | numOfUsedThreads | comment
  printf("matrix_mul,%.3f,%d,%d,none\n", exec_time, problemSize, 1);

  freeMat(A, problemSize);
  freeMat(B, problemSize);
  freeMat(C, problemSize);

  return EXIT_SUCCESS;
}
