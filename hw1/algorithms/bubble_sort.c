/*
 * Sorting of N-element array using bubble sort.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*
 * Sorting algorithm.
 */
void bubble_sort(int *a, int n) {
  int tmp, modified;
  do {
    modified = 0;
    for (int i = 1; i < n; i++) {
      if (a[i] < a[i - 1]) {
        tmp = a[i];
        a[i] = a[i - 1];
        a[i - 1] = tmp;
        modified = 1;
      }
    }
  } while (modified);
}

/*
 * Main program.
 */
int main(int argc, char **argv) {
  if (argc < 2) {
    return EXIT_FAILURE;
  }

  int problemSize = atoi(argv[1]);
  struct timespec start, end;

  srand(time(NULL));

  int* array = (int *)malloc(problemSize * sizeof(int));

  for (int i = 0; i < problemSize; ++i) {
    array[i] = rand();
  }

  clock_gettime(CLOCK_MONOTONIC, &start);

  bubble_sort(array, problemSize);

  clock_gettime(CLOCK_MONOTONIC, &end);
  double exec_time = (end.tv_sec - start.tv_sec);
  exec_time += (end.tv_nsec - start.tv_nsec) / 1000000000.0;

  // print as csv (call it parallel to support the analyse script even though
  // it
  // is single threaded)
  // algo | execTime [s] | problemSize | numOfUsedThreads | comment
  printf("bubble_sort,%.3f,%d,%d,none\n", exec_time, problemSize, 1);

  free(array);

  return EXIT_SUCCESS;
}
