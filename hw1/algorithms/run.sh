#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
cd "$SCRIPT_DIR"

BIN_DIR="./bin/"
OUTPUT_FILE="./analyse.csv"

execute_mpi_task_5_times(){
  #module load mpi
  for count in {0..4} ; do
    for numThreads in {1,2,4} ; do
      echo "run: $1 $2 $numThreads"
      mpirun -n $numThreads $BIN_DIR/$1 $2 >> $OUTPUT_FILE
    done
  done
  #module unload mpi
}

execute_omp_task_5_times(){
  for count in {0..4} ; do
    for numThreads in {1,2,4} ; do
      echo "run: $1 $2 $numThreads"
				$BIN_DIR/$1 $2 $numThreads >> $OUTPUT_FILE
    done
  done
}

execute_task_5_times(){
  for count in {0..4} ; do
    echo "run: $1 $2"
    $BIN_DIR/$1 $2 >> $OUTPUT_FILE
  done
}

if [ -f $OUTPUT_FILE ] ; then
  rm $OUTPUT_FILE
fi

SIEVE_SIZE=150000000
MATRIX_SIZE=1000
BUBBLE_SORT_SIZE=100000

#execute_mpi_task_5_times matrix_mul_mpi $MATRIX_SIZE
#execute_mpi_task_5_times sieve_mpi $SIEVE_SIZE
#execute_mpi_task_5_times bubble_sort_mpi $BUBBLE_SORT_SIZE

#execute_task_5_times matrix_mul $MATRIX_SIZE
#execute_task_5_times sieve $SIEVE_SIZE
#execute_task_5_times bubble_sort $BUBBLE_SORT_SIZE

execute_omp_task_5_times bubble_sort_omp $BUBBLE_SORT_SIZE
