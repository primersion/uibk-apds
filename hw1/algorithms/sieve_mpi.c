/*
 * Determine prime numbers using the sieve of Eratosthenes.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "mpi_util.h"

int count_primes(int n, int *primes) {
  int i;
  int count = 0;

  for (i = 0; i < n; i++) {
    if (!primes[i]) {
      count++;
    }
  }

  return count;
}

void sieve_mpi(int n, int *primes, struct MPIState state) {
  int limit = (int)sqrt((double)n);

  // Make sure workload can be evenly distributed;
  while (((n - limit) % state.num_proc) != 0) {
    limit++;
  }

  int worker_size = (n - limit) / state.num_proc;

  // sieve
  int i, j;
  for (i = 2; i <= limit; i++) {
    if (!primes[i]) {
      // eliminate all non-primes in [1, sqrt(n)]
      for (int j = i * i; j <= limit; j += i) {
        primes[j] = 1;
      }

      // eliminate all non-primes in [sqrt(n) + 1, n]
      int start = (limit + 1) + state.rank_id * worker_size;
      int end = fmin(start + worker_size, n);

      // increment until start is a multiple of i
      while (start % i != 0) {
        start++;
      }

      // eliminate non-primes
      for (j = start; j < end; j += i) {
        primes[j] = 1;
      }
    }
  }
}

int main(int argc, char *argv[]) {
  if (argc < 2) {
    fprintf(stderr, "Usage: %s <problem-size>\n", argv[0]);
    return EXIT_FAILURE;
  }

  // init MPI
  struct MPIState state = initMPI(argc, argv);

  unsigned problem_size = atoi(argv[1]);
  int *primes = (int *)calloc(problem_size, sizeof(int));

  // initialize sieve
  primes[0] = 1;
  primes[1] = 1;

  struct timespec start, end;

  /***************************** Master Task ******************************/
  if (state.rank_id == MPI_MASTER) {
    // start time measuring
    clock_gettime(CLOCK_MONOTONIC, &start);
  }

  sieve_mpi(problem_size, primes, state);

  int *reduced;
  /***************************** Master Task ******************************/
  if (state.rank_id == MPI_MASTER) {
    reduced = (int *)malloc(problem_size * sizeof(int));
  }

  MPI_Reduce(primes, reduced, problem_size, MPI_INT, MPI_BOR, MPI_MASTER,
             MPI_COMM_WORLD);

  /***************************** Master Task ******************************/
  if (state.rank_id == MPI_MASTER) {
    // stop time measuring
    clock_gettime(CLOCK_MONOTONIC, &end);
    double exec_time = (end.tv_sec - start.tv_sec);
    exec_time += (end.tv_nsec - start.tv_nsec) / 1000000000.0;

    // print as csv
    // algo | execTime [ms] | problemSize | numOfUsedThreads | comment
    printf("sieve_mpi,%.3f,%d,%d,Found %d prime-numbers\n", exec_time,
           problem_size, state.num_proc, count_primes(problem_size, reduced));

    free(reduced);
  }

  free(primes);
  MPI_Finalize();
  return EXIT_SUCCESS;
}
