/*
 * MPI matrix multiplication of two NxN matrices.
 */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "mpi_util.h"

unsigned bubbleSort(int *a, unsigned n) {
  unsigned changes = 0;

  for (unsigned i = 1; i < n; ++i) {
    if (a[i] < a[i - 1]) {
      int tmp = a[i];
      a[i] = a[i - 1];
      a[i - 1] = tmp;
      changes++;
    }
  }

  return changes;
}

int checkSorting(int *should, int *is, int problemSize) {
  // Check if array is sorted in ascending order.
  int i;
  for (i = 1; i < problemSize; i++)
    if (is[i - 1] > is[i])
      return 0;

  // Check if sorted array contains all elements of the original array.
  // Because we checked previously if the array is sorted, same elements are
  // located together.
  for (i = 0; i < problemSize; i++) {
    int count = 1;

    // Count ocurrences of current element in sorted array.
    int j;
    for (j = i; j < problemSize - 1; i++, j++)
      if (is[j] == is[j + 1])
        count++;
      else
        break;

    // Check if the current element is[i] occurs in the original array
    // just as often as counted above.
    for (j = 0; j < problemSize; j++)
      if (should[j] == is[i])
        count--;

    // 'count' has to be 0 at that point, otherwise at least one element
    // is missing or appears too often in the sorted array.
    if (count != 0)
      return 0;
  }

  return 1;
}

void printArray(int *a, int n) {
  int i;
  for (i = 0; i < n; i++) {
    printf("%3d ", a[i]);
  }

  putchar('\n');
}

/*
 * Main program.
 */
int main(int argc, char *argv[]) {
  if (argc < 2) {
    fprintf(stderr, "Usage: %s <problem-size>\n", argv[0]);
    return EXIT_FAILURE;
  }

  // Init MPI
  struct MPIState state = initMPI(argc, argv);
  state.msg_type = MPI_INT;

  unsigned problemSize = atoi(argv[1]);
  unsigned workerSize = problemSize / state.num_proc;
  if (workerSize % 2 == 1) { // ensure even size
    workerSize--;
  }

  srand(time(NULL));

  int *array;
  int *comp;
  int *workerChunk;
  int changed = 1;
  struct timespec start, end;

  int *displs = (int *)malloc(state.num_proc * sizeof(int));
  int *counts = (int *)malloc(state.num_proc * sizeof(int));

  /***************************** Master Task ******************************/
  if (state.rank_id == MPI_MASTER) {
    array = (int *)malloc(problemSize * sizeof(int));
    comp = (int *)malloc(problemSize * sizeof(int));

    for (unsigned i = 0; i < problemSize; ++i) {
      // range restricted to improve output
      array[i] = rand() % (10 * problemSize);
      comp[i] = array[i];
    }

    clock_gettime(CLOCK_MONOTONIC, &start);
  }
  /************************************************************************/

  workerChunk = (int *)malloc(workerSize * sizeof(int));

  while (changed) {
    changed = 0;
    for (unsigned odd = 0; odd < 2; odd++) {
      for (int i = 0; i < state.num_proc; ++i) {
        if (odd == 0) {
          displs[i] = i * workerSize;
        } else {
          if (i < 1) {
            displs[i] = 0;
          } else {
            displs[i] = i * workerSize - odd;
          }
        }

        counts[i] = workerSize;
      }

      // Every process gets its chunk of work
      MPI_Scatterv(array, counts, displs, MPI_INT, workerChunk, workerSize,
                   MPI_INT, MPI_MASTER, MPI_COMM_WORLD);

      // Sort chunks
      changed += bubbleSort(workerChunk, workerSize);

      /***************************** Master Task
      ******************************/
      if (state.rank_id == MPI_MASTER) {
        // Master doing columns left if problem size was not evenly divisible
        int sizeLeft = problemSize - (workerSize * state.num_proc) + odd;
        if (sizeLeft != 0) {
          int start = problemSize - sizeLeft;
          changed += bubbleSort(array + start, sizeLeft);
        }
      }
      /************************************************************************/

      // Gather results
      MPI_Gatherv(workerChunk, workerSize, MPI_INT, array, counts, displs,
                  MPI_INT, MPI_MASTER, MPI_COMM_WORLD);
    }

    int changes = 0;
    MPI_Allreduce(&changed, &changes, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    changed = changes;
  }

  /***************************** Master Task ******************************/
  if (state.rank_id == MPI_MASTER) {
    clock_gettime(CLOCK_MONOTONIC, &end);
    double exec_time = (end.tv_sec - start.tv_sec);
    exec_time += (end.tv_nsec - start.tv_nsec) / 1000000000.0;

    // print as csv
    // algo | execTime [s] | problemSize | numOfUsedThreads | comment
    printf("bubble_sort_mpi,%.3f,%d,%d,%s\n", exec_time, problemSize,
           state.num_proc,
           checkSorting(comp, array, problemSize) ? "TRUE" : "FALSE");

    free(array);
    array = NULL;
    free(comp);
    comp = NULL;
  }
  /************************************************************************/

  free(workerChunk);
  workerChunk = NULL;
  free(displs);
  displs = NULL;
  free(counts);
  counts = NULL;

  MPI_Finalize();
  return EXIT_SUCCESS;
}
