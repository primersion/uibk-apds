/*
 * MPI matrix multiplication of two NxN matrices.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "matrix_util.h"
#include "mpi_util.h"

/*
 * Matrix multiplication with A = row x row and B,C = row x col.
 */
void matrixMul(int rows, int cols, int start_col, double **A, double **B,
               double **C) {
  int i, j, k;
  for (i = 0; i < rows; i++) {
    for (j = start_col; j < cols; j++) {
      C[i][j] = 0;
      for (k = 0; k < rows; k++) {
        C[i][j] += A[i][k] * B[k][j];
      }
    }
  }
}

/*
 * Main program.
 */
int main(int argc, char *argv[]) {
  if (argc < 2) {
    fprintf(stderr, "Usage: %s <problem-size>\n", argv[0]);
    return EXIT_FAILURE;
  }

  // Init MPI
  struct MPIState state = initMPI(argc, argv);
  state.msg_type = MPI_DOUBLE;

  unsigned problem_size = atoi(argv[1]);
  int worker_size = problem_size / state.num_proc;
  double **A, **B, **C;
  struct timespec start, end;

  srand(time(NULL));

  /***************************** Master Task ******************************/
  if (state.rank_id == MPI_MASTER) {
    A = allocateMat(problem_size, problem_size);
    B = allocateMat(problem_size, problem_size);
    C = allocateMat(problem_size, problem_size);
    initMat(A, problem_size, problem_size);
    initMat(B, problem_size, problem_size);
    clock_gettime(CLOCK_MONOTONIC, &start);
  }
  /************************************************************************/

  /***************************** Worker Task ******************************/
  if (state.rank_id != MPI_MASTER) {
    A = allocateMat(problem_size, problem_size);
    B = allocateMat(problem_size, worker_size);
    C = allocateMat(problem_size, worker_size);
  }
  /************************************************************************/

  // All processes get complete matrix A
  for (unsigned i = 0; i < problem_size; i++) {
    MPIBroadcast(&state, A[i], problem_size, MPI_MASTER);
  }

  // Every process gets worker_size columns of matrix B
  for (unsigned i = 0; i < problem_size; i++) {
    MPIScatter(&state, B[i], B[i], worker_size, MPI_MASTER);
  }

  // Calculate partial results for A x B = C
  matrixMul(problem_size, worker_size, 0, A, B, C);

  /***************************** Master Task ******************************/
  if (state.rank_id == MPI_MASTER) {
    // Master doing columns left if problem size was not evenly divisible
    int cols_left = problem_size - (worker_size * state.num_proc);
    if (cols_left != 0) {
      int start_col = problem_size - cols_left;
      matrixMul(problem_size, problem_size, start_col, A, B, C);
    }
  }
  /************************************************************************/

  // Gather results
  for (unsigned i = 0; i < problem_size; i++) {
    MPIGather(&state, C[i], C[i], worker_size, MPI_MASTER);
  }

  /***************************** Master Task ******************************/
  if (state.rank_id == MPI_MASTER) {
    clock_gettime(CLOCK_MONOTONIC, &end);
    double exec_time = (end.tv_sec - start.tv_sec);
    exec_time += (end.tv_nsec - start.tv_nsec) / 1000000000.0;

    // print as csv (call it parallel to support the analyse script even though
    // it
    // is single threaded)
    // algo | execTime [s] | problemSize | numOfUsedThreads | comment
    printf("matrix_mul_mpi,%.3f,%d,%d,none\n", exec_time, problem_size,
           state.num_proc);
  }
  /************************************************************************/

  freeMat(A, problem_size);
  freeMat(B, problem_size);
  freeMat(C, problem_size);

  // if(state.rank_id == MPI_MASTER) {
  //   printMat(A, problem_size, problem_size);
  //   printMat(B, problem_size, problem_size);
  //   printMat(C, problem_size, problem_size);
  // }

  MPI_Finalize();
  return EXIT_SUCCESS;
}
