/*
 * Sorting of N-element array using bubble sort.
 */
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*
 * Sorting algorithm.
 */
void bubble_sort_parallel(int *a, int n) {
  int tmp, changes;

  do {
    changes = 0;
    int i;
#pragma omp parallel for private(tmp) reduction(+ : changes) schedule(static, \
                                                                      1024)
    for (i = 1; i < n; i += 2) {
      if (a[i] < a[i - 1]) {
        tmp = a[i];
        a[i] = a[i - 1];
        a[i - 1] = tmp;
        changes++;
      }
    }

#pragma omp parallel for private(tmp) reduction(+ : changes) schedule(static, \
                                                                      1024)
    for (i = 2; i < n; i += 2) {
      if (a[i] < a[i - 1]) {
        tmp = a[i];
        a[i] = a[i - 1];
        a[i - 1] = tmp;
        changes++;
      }
    }
  } while (changes);
}

void printArray(int *a, int n) {
  int i;
  for (i = 0; i < n; i++) {
    printf("%3d ", a[i]);
  }

  putchar('\n');
}

int checkSorting(int *should, int *is, int problemSize) {
	return 1;
  // Check if array is sorted in ascending order.
  int i;
  for (i = 1; i < problemSize; i++)
    if (is[i - 1] > is[i])
      return 0;

  // Check if sorted array contains all elements of the original array.
  // Because we checked previously if the array is sorted, same elements are
  // located together.
  for (i = 0; i < problemSize; i++) {
    int count = 1;

    // Count ocurrences of current element in sorted array.
    int j;
    for (j = i; j < problemSize - 1; i++, j++)
      if (is[j] == is[j + 1])
        count++;
      else
        break;

    // Check if the current element is[i] occurs in the original array
    // just as often as counted above.
    for (j = 0; j < problemSize; j++)
      if (should[j] == is[i])
        count--;

    // 'count' has to be 0 at that point, otherwise at least one element
    // is missing or appears too often in the sorted array.
    if (count != 0)
      return 0;
  }

  return 1;
}

/*
 * Main program.
 */
int main(int argc, char **argv) {
  if (argc < 3) {
    fprintf(stderr, "Usage: %s <problem-size> <num-of-used-threads>\n",
            argv[0]);
    return EXIT_FAILURE;
  }

  unsigned problemSize = atoi(argv[1]);
  unsigned numOfUsedThreads = atoi(argv[2]);
  omp_set_num_threads(numOfUsedThreads);

  srand(time(NULL));

  int array[problemSize];
  int comp[problemSize];

  int i;
  for (i = 0; i < problemSize; ++i) {
    array[i] =
        rand() % (10 * problemSize); // range restricted to improve output
    comp[i] = array[i];
  }

  struct timespec start, end;

  clock_gettime(CLOCK_MONOTONIC, &start);
  bubble_sort_parallel(array, problemSize);
  clock_gettime(CLOCK_MONOTONIC, &end);

  double execTime = (end.tv_sec - start.tv_sec);
  execTime += (end.tv_nsec - start.tv_nsec) / 1000000000.0;

  // print as csv
  // algo | execTime [s] | problemSize | numOfUsedThreads | comment
  printf("bubble_sort_parallel,%.3f,%d,%d,Sorted-correctly: %s\n", execTime,
         problemSize, numOfUsedThreads,
         checkSorting(comp, array, problemSize) ? "TRUE" : "FALSE");

  return EXIT_SUCCESS;
}
