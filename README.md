# Advanced Parallel and Distributed Systems Proseminar (SS 2017)
### Clemens Degasper, Manfred Moosleitner, Philipp Stiegernigg, Alexander Jungmann

This repository contains all material created for the proseminar **Advanced Parallel and Distributed Systems** in **SS 2017**.


### Directory Structure
The repository is structured as follows:

- **aws_samples** - contains sample Java programs for using Amazon EC2, Lambda and S3
- **hw1** - contains the first homework
- **lecture** - contains some material regarding the lecture
- **project** - contains the main contribution, which is the project developed throughout the course

### Running the CloudBroker
To try out our Cloud Broker import the 'CloudBroker' project into eclipse. Make sure that you have your AWS credentials set up. Next copy the content of the 'log' folder (only t2.micro instances) or the content of one of the folders in 'benchmark/results_uibk' to a folder called 'log' inside your home directory. You can now run the class CloudBroker.java and follow the instructions. Make sure that if you are creating new benchmark data, you have sufficient funds on your Amazon account or set testMode inside the CloudBroker class to true (only use t2.micro instances). Otherwise you will be charged $$$.
